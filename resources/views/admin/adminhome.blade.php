@extends('layouts.master')
@section('stylesheets')
    {{--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#invoices_table').dataTable({
                "order": [[ 1, "desc" ]]
            } );
        });
    </script>

@endsection

@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script>
        var StripeKey = $("#STRIPE_KEY").val();
    </script>
    {{ Html::script('src/js/dashboard.js') }}

    @endsection
<div class="container-fluid jumbotron td-jumbotron">
    <div class="container">  <h2>Dashboard</h2></div>
</div>

{{--{{ var_dump($orders) }}--}}
@section('content')

    <?php

    if (isset($section)){
        switch ($section) {

            case "listings":
                $listingsTab = 'in active';
                $listingLink = 'active';
                break;
            case "settings":
                $settingsTab = 'in active';
                $settingsLink = 'active';

                break;
            case "billing":
                $billingTab = 'in active';
                $billingLink = 'active';
                break;
            case "membership":
                $membershipTab = 'in active';
                $membershipLink = 'active';
                break;
            case "order-history":
                $orderHistoryTab = 'in active';
                $orderHistoryLink = 'active';
                break;
            default:
                $profileTab = 'in active';
                $profileLink = 'active';
        }
    } else {

        $profileTab = 'in active';
        $profileLink = 'active';
    }


            ?>




    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div style="margin-top:-72px;">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="{{ $profileLink or ""}}"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                        <li role="presentation" class="{{ $listingLink or "" }}"><a href="#listings" aria-controls="listings" role="tab" data-toggle="tab">Your listings</a></li>
                        <li role="presentation" class="{{ $membershipLink or "" }}"><a href="#membership" aria-controls="membership" role="tab" data-toggle="tab">Membership</a></li>
                        <li role="presentation" class="{{ $settingsLink or "" }}"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                        <li role="presentation" class="{{ $billingLink or "" }}"><a href="#billing" aria-controls="billing" role="tab" data-toggle="tab">Billing</a></li>
                        <li role="presentation" class="{{ $orderHistoryLink or "" }}"><a href="#order_history" aria-controls="order_history" role="tab" data-toggle="tab">Order History</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade {{ $profileTab or "" }}" id="profile">
                            @include('includes.dashboard._profile')
                        </div>
                        <div role="tabpanel" class="tab-pane fade {{ $listingsTab or "" }}" id="listings">
                            @include('includes.dashboard._listings')
                        </div>
                        <div role="tabpanel" class="tab-pane fade {{ $membershipTab or "" }}" id="membership">
                            @include('includes.dashboard._membership')
                        </div>
                        <div role="tabpanel" class="tab-pane fade {{ $settingsTab or "" }}" id="settings">
                            @include('includes.dashboard._settings')
                        </div>
                        <div role="tabpanel" class="tab-pane fade {{ $billingTab or "" }}" id="billing">
                            @include('includes.dashboard._billing')
                        </div>
                        <div role="tabpanel" class="tab-pane fade {{ $orderHistoryTab or "" }}" id="order_history">
                            @include('includes.dashboard._invoices')
                        </div>



                    </div>

                </div>



            </div>
        </div>
    </div>


@endsection
{{--@section('scripts')--}}

    <script type="text/javascript">
        Stripe.setPublishableKey('pk_test_L3EOH6Z8NAHkccCdmSwn1z4f');
    </script>

    {{ Html::script('src/js/dashboard.js') }}
{{--@endsection--}}