@extends('layouts.master')
@section('content')
    @section('title', $aircraft->make .' '. $aircraft->model .' - ' . $aircraft->tail_number . ' - Ex-'.$aircraft->ex_airline )
    <div class="container-fluid jumbotron td-jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <dl class="list-unstyled">
                        
                        <dd><h2>{{ $aircraft->make }} {{ $aircraft->model }}<p></p>

                        <small>Tail Number {{ $aircraft->tail_number }}</small></h2></dd>
                    </dl>

                </div>
                <div class="col-sm-4">
                <h5>Listed by</h5>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="..." alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">TAG Aero</h4>
                            TAG Aero is the leading manufactuerer of etc..
                            <p></p>
                            <i class="fa fa-phone" aria-hidden="true"></i> +1 407-614-8314 | <i class="fa fa-envelope" aria-hidden="true"></i> info@tag.aero
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<div class="container">
{{--     @if (Auth::check())
    @if( Auth::user()->id == $aircraft->owned_by )
    <div class="row well">
        <div class="col-sm-6">
           <h4>This is your listing. It will expire in {{ $aircraft->days_to_go }} days.</h4>
        </div>
        <div class="col-sm-6">
            <a href="{{ route('aircraft.edit', $aircraft->id) }}" class="btn btn-default pull-right">Edit</a>
        </div>
    </div>
    @endif
    @endif --}}



    <div class="row">


    <div class="col-sm-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                <strong>Engines</strong>
            </div>

            <div class="panel-body">           
                 <dl class="dl-horizontal">

                    <dt>Make</dt>
                    <dd>{{ $aircraft->make }}</dd>

                    <dt>Model</dt>
                    <dd>{{ $aircraft->model }}</dd>

                    <dt>Serial Number</dt>
                    <dd>{{ $aircraft->serial }}</dd>
                
                   
                </dl>
            </div>

        </div>
        </div>





            <div class="col-sm-12">


        <div class="panel panel-default">

            <div class="panel-heading">
                <strong>Engines</strong>
            </div>

            <div class="panel-body">           
                    <p>
                    {{ $aircraft->engines }}
                    </p>
            </div>

        </div>

            </div>


            <div class="col-sm-12 ">
                
              <div class="panel panel-default">

            <div class="panel-heading">
                <strong>APUs</strong>
            </div>

            <div class="panel-body">           
                    <p>
                    {{ $aircraft->apu }}
                    </p>
            </div>

        </div>
            </div>


            <div class="col-sm-12">
                
                       <div class="panel panel-default">

            <div class="panel-heading">
                <strong>Avionics</strong>
            </div>

            <div class="panel-body">           
                    <p>
                    {{ $aircraft->avionics }}
                    </p>
            </div>

        </div>


            </div>

            <div class="col-sm-12 ">
                
               <div class="panel panel-default">

            <div class="panel-heading">
                <strong>Landing Gear</strong>
            </div>

            <div class="panel-body">           
                    <p>
                    {{ $aircraft->landing_gear }}
                    </p>
            </div>

        </div>


            </div>

</div>


        </div>



@endsection