@extends('layouts.master')
@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
@endsection
@section('title','Aircrafts in Tear-down')
@section('content')

	<div class="container">


	<h1>Aircraft</h1>

		<div class="row">
		<div class="col-sm-12">

		 <table id="listings" class="display" cellspacing="0" width="100%">
			<thead>

			<th>Manufacturer</th>
			<th>Model</th>
			<th>Tail No.</th>
			<th>MSN</th>
			<th>Last Operator</th>
			
			<th></th>


			</thead>


			<tbody>
		@foreach($aircrafts as $aircraft)

			<tr>
				<td>{{ $aircraft->make }}</td>
				<td>{{ $aircraft->model }}</td>
				<td>{{ $aircraft->tail_number }}</td>
				<td>{{ $aircraft->serial }}
				<td>{{ $aircraft->ex_airline }}</td>
				<td><a href="{{ route('aircraft.show', $aircraft->id) }}">View</a></td>
			</tr>


		@endforeach
			</tbody>

		</table>

		</div>
		</div>


	</div>



@endsection