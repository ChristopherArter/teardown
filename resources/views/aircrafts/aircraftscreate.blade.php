@extends('layouts.master')

@section('content')

 <div class="container">
     <div class="row">
         <div class="col-sm-3">
             <h3>Title area?</h3>
         </div>

         <div class="col-sm-8 col-sm-offset-1">


        <div class="row">

         @if($method == 'post')
{!! Form::open(['method' => 'POST', 'url' => '/aircraft', 'class' => 'form']) !!}


@elseif($method=='put')
{!! Form::model($aircraft, ['route' => ['aircraft.update', $aircraft->id], 'method' => 'PUT', 'class' => 'form']) !!}
@endif

             <div class="panel">
             <div class="panel-body">
<legend>Aircraft Details</legend>
    {{-- Make --}}
             <div class="col-sm-6">
                 <div class="form-group{{ $errors->has('make') ? ' has-error' : '' }}">
                    {!! Form::label('make', 'Manufacturer') !!}
                    {!! Form::text('make', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('make') }}</small>
                </div>
            </div>


    {{-- Model --}}
            <div class="col-sm-6">
                <div class="form-group{{ $errors->has('model') ? ' has-error' : '' }}">
                    {!! Form::label('model', 'Model') !!}
                    {!! Form::text('model', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('model') }}</small>
                </div>
            </div>


    {{-- Serial --}}
            <div class="col-sm-6">
            <div class="form-group{{ $errors->has('serial') ? ' has-error' : '' }}">
                {!! Form::label('serial', 'MSN') !!}
                {!! Form::text('serial', null, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('serial') }}</small>
            </div>
            </div>

    {{-- Tail Number --}}
            <div class="col-sm-6">
                <div class="form-group{{ $errors->has('tail_number') ? ' has-error' : '' }}">
                    {!! Form::label('tail_number', 'Registration / Tail No.') !!}
                    {!! Form::text('tail_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('tail_number') }}</small>
                </div>
            </div>

    {{-- Previous Operator / Ex-Airline --}}
            <div class="col-sm-6">

            <div class="form-group{{ $errors->has('ex_airline') ? ' has-error' : '' }}">
                    {!! Form::label('ex_airline', 'Previous Operator') !!}
                    <div class="input-group">
                    <div class="input-group-addon">Ex-</div>
                    {!! Form::text('ex_airline', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    </div>
                    
                    <small class="text-danger">{{ $errors->first('ex_airline') }}</small>
                </div>
            </div>







    {{--   Location --}}

            <div class="col-sm-6">
            <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                {!! Form::label('location', 'Location') !!}

                {!! Form::textarea('location', null, ['class' => 'form-control', 'required' => 'required', 'rows' => 4]) !!}
                <p class="help-block">Either address, airport, city, etc.</p>
                <small class="text-danger">{{ $errors->first('location') }}</small>
            </div>
            </div>





        </div>
         </div>

             <div class="panel">
             <div class="panel-body">
                <legend>Material Details</legend>


                {{-- Avionics --}}
                <div class="col-sm-12">
                <div class="form-group{{ $errors->has('avionics') ? ' has-error' : '' }}">
                    {!! Form::label('avionics', 'Avionics') !!}
                    {!! Form::textarea('avionics', null, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('avionics') }}</small>
                </div>
                </div>


                {{-- Engines --}}

                <div class="col-sm-12">
                <div class="form-group{{ $errors->has('engines') ? ' has-error' : '' }}">
                    {!! Form::label('engines', 'Engines') !!}
                    {!! Form::textarea('engines', null, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('engines') }}</small>
                </div>
                </div>

                {{-- Apu --}}

                <div class="col-sm-12">
                <div class="form-group{{ $errors->has('apu') ? ' has-error' : '' }}">
                        {!! Form::label('apu', 'APU(s)') !!}
                        {!! Form::textarea('apu', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('apu') }}</small>
                    </div>
                </div> 

                {{-- Landing Gear --}}

                <div class="col-sm-12">
                <div class="form-group{{ $errors->has('landing_gear') ? ' has-error' : '' }}">
                        {!! Form::label('landing_gear', 'Landing Gear') !!}
                        {!! Form::textarea('landing_gear', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('landing_gear') }}</small>
                    </div>
                </div>


                {{-- Submit Button --}}
                <div class="col-sm-12">
                    <div class="btn-group pull-right">
                        {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                        {!! Form::submit("Save", ['class' => 'btn btn-Add']) !!}
                    </div>
                </div>


             </div>
             </div>



        {!! Form::close() !!}
 
 
     </div>

</div>
 </div>

    @endsection