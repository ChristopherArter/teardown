<!doctype html>
        <html lang="en">
@include('includes._header')
@include('includes._navigation')
<body>

<div class="main">
    @yield('content')
</div>
@include('includes._footer')
@yield('scripts')
</body>
</html>