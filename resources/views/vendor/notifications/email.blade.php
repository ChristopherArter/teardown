@extends('mail.mastermail')


@section('content')

                                           <h1>
                                            @if (! empty($greeting))
                                                {{ $greeting }}
                                            @else
                                                @if ($level == 'error')
                                                    Whoops!
                                                @else
                                                    Hello!
                                                @endif
                                            @endif
                                        </h1>

                                        <!-- Intro -->
                                        @foreach ($introLines as $line)
                                            <p>
                                                {{ $line }}
                                            </p>
                                        @endforeach

                                        <!-- Action Button -->
                                        @if (isset($actionText))

                                            <?php
                                            switch ($level) {
                                                case 'success':
                                                    $actionColor = 'button--green';
                                                    break;
                                                case 'error':
                                                    $actionColor = 'button--red';
                                                    break;
                                                default:
                                                    $actionColor = 'button--blue';
                                            }
                                            ?>

                                            <a href="{{ $actionUrl }}"
                                               style=""
                                               class="button"
                                               target="_blank">
                                                {{ $actionText }}
                                            </a>

                                        @endif

                                        <!-- Outro -->
                                        @foreach ($outroLines as $line)
                                            <p>
                                                {{ $line }}
                                            </p>
                                        @endforeach


                                        <!-- Sub Copy -->
                                        @if (isset($actionText))

                                                        <p>
                                                            If you’re having trouble clicking the "{{ $actionText }}" button,
                                                            copy and paste the URL below into your web browser:
                                                        </p>

                                                        <p>
                                                            <a href="{{ $actionUrl }}" target="_blank">
                                                                {{ $actionUrl }}
                                                            </a>
                                                        </p>

                                        @endif
                               @endsection