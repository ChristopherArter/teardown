@extends('layouts.master')

@section('content')

    <div class="container-fluid jumbotron td-jumbotron">
        <div class="container">  <h2>Marketplace<p></p> <small>A free marketplace for aviation materials</small></h2></div>
    </div>
    <div class="container">


        <p></p>
        <div class="row">
            <div class="col-sm-3">

            <button type="button" class="btn-block btn btn-primary" data-toggle="modal" data-target="#td_market_create_post_modal">
                <i class="fa fa-bullhorn" aria-hidden="true"></i> Create Post
            </button>

            <!-- Modal -->
            <div class="modal fade" id="td_market_create_post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Market Post</h4>
                        </div>
                        <div class="modal-body">

                            @if (Auth::check())


                            <form action="{{ route('market.store') }}" method="POST" id="marketSubmitForm">
                                {{--csrf token--}}
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                {{--Market title--}}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Market Post Title</label>
                                    <input type="text" name="market_title" class="form-control" id="td_market_title_input" placeholder="Market Post Title">
                                <small><span class="pull-right help-block" id="count_message"></span></small>
                                </div>


                                {{--Market body--}}
                                <div class="form-group">
                                    <label>Body</label>
                                    <textarea rows="4" name="market_body" class="form-control" id="td_market_body_input" placeholder="Market Post Content"></textarea>
                                    <small><span class="text-small pull-right help-block" id="count_message_body"></span></small>
                                </div>

                                {{--Market post type--}}
                                <div class="form-group">
                                    <label for="market_type">Post Type:</label>

                                        <select class="form-control" name="market_type">
                                            @foreach($marketPostTypes as $key=>$value )
                                            <option value="{{ $key }}">{{ $value }}</option>
                                                @endforeach

                                        </select>
                                </div>

                                {{--Market category--}}
                                <div class="form-group">
                                    <label for="market_cat">Category:</label>

                                    <select class="form-control" name="market_cat">
                                        @foreach($marketCategories as $category)
                                        <option value="{{ $category }}">{{ $category }}</option>

                                            @endforeach
                                    </select>
                                </div>


                                <button id="market_submit_button" type="submit" class="btn btn-primary">Submit</button> <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </form>


                                @else
                                @include('includes._signinorsignup')
                                @endif

                        </div>

                    </div>
                </div>
            </div>



<p></p>

            <div class="well">

                <div class="form-group">
                    <div class="input-group">

                    <input autocomplete="off" type="text" class="form-control"  name="market_param_text" id="market_param_keyword" placeholder="Keyword">
                        <div id="market_param_keyword_clear" class="input-group-addon btn"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                    </div>
                </div>

                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button id="market_check_all_button"type="button" class="btn btn-sm btn-default"><i class="fa fa-check-square-o" aria-hidden="true"></i> Check All</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button id="market_uncheck_all_button" type="button" class="btn btn-sm btn-default"><i class="fa fa-square-o" aria-hidden="true"></i> Uncheck All</button>
                    </div>
                </div>



<p></p>

            <strong>Post Type</strong>

                @foreach($marketPostTypes as $key=>$value )
                    <div class="checkbox">
                        <label>
                    <input type="checkbox" class="td-market-post-type-choice" value="{{ $key }}" checked> {{ $value }}
                            </label>
                        </div>
                @endforeach

                <strong>Categories</strong>
                @foreach ($marketCategories as $category)

                    <div class="checkbox">
                        <label>
                            <input value="{{ $category }}" class="td-market-category-choice" type="checkbox" checked> {{ $category }}
                        </label>
                    </div>
                    @endforeach

                <button type="submit" id="td_market_query_submit" class="btn btn-default btn-sm btn-block"><i class="fa fa-refresh" aria-hidden="true"></i> Update</button>





            </div>
        </div>

        <div class="col-sm-6" id="td_market_content_loader">


@include('includes.market._marketcontent')


        </div>

            <div class="col-sm-3">
               <h4>Stuff here?</h4>
                <hr>

                maybe we need some stuff here?

            </div>


    </div>
    </div>

    @endsection
@section('scripts')
    {{ Html::script('src/js/market.js') }}
    @endsection