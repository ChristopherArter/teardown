

<div class="media">
  <div class="media-left">
    <a href="{{ route(''. $listing->post_type .'s.show', $listing->id ) }}">
      <img class="media-object thumbnail" style="max-width:64px; max-height: 50px;" src="" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">

    <a href="{{ route(''. $listing->post_type .'s.show', $listing->id ) }}">{{ $listing->model }} 

    @if($listing->post_type=="engine")

{{ $listing->minor_variant }}

    @endif


    | {{ $listing->condition }} Condition</a></h4>
    <p><small>Listed by <strong>TAG Aero</strong></small><p>




  </div>
</div>