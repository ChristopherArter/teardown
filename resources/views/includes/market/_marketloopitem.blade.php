<div class="row td-market-loop-item" >
<?php

//    $fav = DB::table('favorites')->where('post_id', '=', $item->id)->where('created_by','=', Auth::user()->id)->get();

        $fav = \App\Favorite::where('post_id', '=', $item->id)->where('created_by','=', Auth::user()->id)->get();
?>


    <div class="col-sm-9"

        <h5><strong>{{ $item->market_title  }} - {{ $item->id }}</strong></h5>
        <span class="label @if($item->market_type == 'wanted') {{ 'label-success' }} @else {{ 'label-default' }} @endif" style="text-transform: capitalize"> {{ str_replace('_', ' ', $item->market_type) }}</span>
        <p>{{ $item->market_body }}</p>
        <p>
            <strong>
        </p>
        <small>Listed by {{ $item-> author_id }} on {{ $item->created_at }}. <strong>{{ $item->market_cat }}</strong></small>
        <hr>
    </div>
    <div class="col-sm-3">

            {{--If this is a favorited post--}}
            @if(!$fav->isEmpty())
                @foreach($fav as $favorite)
            <a href="#" class="td-market-save btn btn-default btn-xs" data-fav="{{ $favorite->post_id }}"><i class="fa fa-star" style="color:gold;" aria-hidden="true"></i> Saved</a>
          @endforeach
                @else
            <a href="#" class="td-market-save btn btn-default btn-xs" data-fav="{{ $item->id }}"><i class="fa fa-star-o" aria-hidden="true"></i> Save</a>
            </strong>
@endif


    </div>


</div>