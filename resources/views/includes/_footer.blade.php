

<div class="container-fluid" style="background:#F4D03F; padding:15px;" >
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <strong>Get FREE weekly emails of listings and As-Removed materials</strong>
            </div>
            <div class="col-sm-6">
                {!! Form::open([

        'url' => '',
        'class'=>'listing-form form-inline',
        'method'=>'post']


    ) !!}

                <div class="form-group">
                    <label class="sr-only" for="exampleInputPassword3">Name</label>
                    <input type="text" class="form-control" id="email_name" placeholder="Name">
                </div>


                <div class="form-group">
                    <label class="sr-only" for="email_email">Email address</label>
                    <input type="email" class="form-control" id="email_email" placeholder="Email">
                </div>


                <button type="submit" class="btn btn-default">Sign-up</button>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="footer">




    <div class="container">


        <div class="clearfix">

            <div class="footer-logo">


                {{--LOGO--}}
                <a href="{{ route('home') }}"><img class="td-aero-nav-logo" src="/src/images/teardown-aero-logo.svg" alt="Teardown.AERO" onerror="this.src='/src/images/teardown-aero-logo.png'; this.onerror=null;" /></a>
            </div>
            <dl class="footer-nav">
                <dt class="nav-title">NAVIGATION</dt>
                <dd class="nav-item"><a href="#">Aircraft</a></dd>
                <dd class="nav-item"><a href="#">Engines</a></dd>
                <dd class="nav-item"><a href="#">APUs</a></dd>
                <dd class="nav-item"><a href="#">Market</a></dd>

            </dl>
            <dl class="footer-nav">
                <dt class="nav-title">ABOUT</dt>
                <dd class="nav-item"><a href="#">The Company</a></dd>
                <dd class="nav-item"><a href="#">Blog</a></dd>
                <dd class="nav-item"><a href="#">Careers</a></dd>
            </dl>
            <dl class="footer-nav">
                <dt class="nav-title">GALLERY</dt>
                <dd class="nav-item"><a href="#">Flickr</a></dd>
                <dd class="nav-item"><a href="#">Picasa</a></dd>
                <dd class="nav-item"><a href="#">iStockPhoto</a></dd>
                <dd class="nav-item"><a href="#">PhotoDune</a></dd>
            </dl>
            <dl class="footer-nav">
                <dt class="nav-title">CONTACT</dt>
                <dd class="nav-item"><a href="#">Basic Info</a></dd>
                <dd class="nav-item"><a href="#">Map</a></dd>
                <dd class="nav-item"><a href="#">Conctact Form</a></dd>
            </dl>
        </div>
        <div class="footer-copyright text-center">Copyright © 2017 Teardown Aero LLC. All rights reserved.</div>
    </div>


</div>


{{ Html::script('/js/app.js') }}


 {{--<!-- FONT AWESOME -->--}}
    <script src="https://use.fontawesome.com/7f52f39042.js"></script>

    <script>
    var token = '{{ Session::token() }}';
    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(100);
    });

    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $('.dropdown').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100);
    });

</script>


