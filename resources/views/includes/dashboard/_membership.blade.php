<div class="container">
    <div class="row">
        <legend><h4>Membership</h4></legend>
    </div>
<?php

    $startDate = Carbon::createFromTimestamp($user->subscription()->created)->toFormattedDateString();
    $renewalDate = Carbon::createFromTimestamp($user->subscription()->current_period_end)->toFormattedDateString();

    ?>
    {{--Membership control sidebar--}}
    <div class="col-sm-3 well">

      <strong>Membership Information</strong>
        <hr>
        <dl>

            <dt><small>Status</small></dt>

            <dd class="text-capitalize">{{ $user->subscription()->status }}</dd>
            <p></p>

            <dt><small>Membership Level</small></dt><p></p>
            <dd><span class="label label-success">Premium - $499.99 Month</span></dd>
            <p></p>
            <dt><small>Renewal Date</small></dt>
            <dd>{{ $renewalDate }}</dd>

            <p></p>
            <dt><small>Subscription Start</small></dt>
            <dd>{{ $startDate }}</dd>

        </dl>

        <p></p>
        <small><a class="btn btn-danger btn-xs" href="#">Cancel Subscription</a></small>
    </div>


    {{--Membership content area--}}

    <div class="col-sm-9">
 <?php

           foreach($userInvoices->data as $sub){
                if(!$sub->subscription == null){
                    var_dump($sub->subscription);

                    ?>
        <table class="table" id="membership_invoices_table">

            <thead>
            <th>

            </th>
            </thead>


        </table>


        <?php




                }


           }

        ?>
    </div>
</div>

