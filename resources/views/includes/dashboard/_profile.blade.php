<div class="container">
    <div class="row">
       <legend><h4>Profile</h4></legend>
    </div>

    <div class="row">
        <div class="col-sm-3 well">
here is profile pictgure
        </div>
        <div class="col-sm-9">
            <dl class="dl-horizontal">
                <dt>Name</dt>
                <dd>{{ $user->name }}</dd>

                <dt>Email</dt>
                <dd style="text-transform: lowercase;">{{ $user->email }} <small>(<a href="#">edit</a>) </small></dd>

                <dt>Company</dt>
                <dd>TAG Aero</dd>
            </dl>
        </div>
    </div>
</div>
