<div class="container">
    <div class="row">
        <legend><h4>Listings Management</h4></legend>
    </div>

    <div class="row">

        <div class="col-xs-2"> <!-- required for floating -->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs tabs-left">
                <li class="active"><a href="#engines" data-toggle="tab">Engines
                        @if(!$engines->isEmpty())
                            <span class="label pull-right label-default">{{ $engines->count() }}</span></a></li>
                @endif

                </a></li>
                <li><a href="#apus" data-toggle="tab">APUs

                        @if(!$apus->isEmpty())

                            <span class="label pull-right label-default">{{ $apus->count() }}</span>

                        @endif

                    </a></li>
                <li><a href="#aircraft" data-toggle="tab">Aircraft</a></li>
                <li><a href="#market_posts" data-toggle="tab">Market Posts</a></li>
            </ul>
        </div>

        <div class="col-xs-10">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="engines">@include('includes.dashboard.partials._listingstables', ['units'=>$engines, 'title'=>'Engines']) </div>
                <div class="tab-pane fade" id="apus">  @include('includes.dashboard.partials._listingstables', ['units'=>$apus, 'title'=>'Teardowns']) </div>
                <div class="tab-pane fade" id="aircraft">AIRCRAFT</div>
                <div class="tab-pane fade" id="market_posts">MARKET POSTS</div>
            </div>
        </div>
    </div>
    </div>

