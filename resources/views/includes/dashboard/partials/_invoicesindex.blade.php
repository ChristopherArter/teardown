<table class="table" id="invoices_table">
    <thead>
    <tr>
        <th>Invoice Number</th>
        <th>Created</th>
        <th>Status</th>

        <th>Total</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>

    @foreach($invoices as $invoice)
    <tr>
        <td>{{ $invoice->invoice_number }}</td>
        <td>
             {{ $invoice->created_at }}
              </td>
        <td>{{ $invoice->status }}</td>

        <td>
                ${{ $invoice->amount_due / 100 }}

        </td>
        <td>

            <!-- Single button -->
            <div class="btn-group">
                <button type="button" class="btn btn-default btn-group-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Options <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">View</a></li>
                    <li><a href="{{ route('dashboard.billing.download', $invoice->id) }}"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download</a></li>
                    <li><a href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></li>
                </ul>
            </div>

</td>

    </tr>
    @endforeach
    </tbody>




</table>