
@section('scripts')

    {{ Html::script('/src/js/vendor/parsley.js') }}
    <script type="text/javascript">
        $('.td-dash-cc-form').parsley();
    </script>

@endsection
{{--FORM START--}}
{!! Form::open([

        'url' => route('dashboard.addCreditCard'),
        'class'=>'form-horizontal',
        'id'=>'td-dash-cc-form',
        'method'=> 'POST',
        'data-parsley-validate' ]


) !!}
<div class="payment-errors"></div>
<fieldset>



    {{--NAME ON CARD--}}
    <div class="form-group" id="">
        <label class="col-sm-3 control-label" for="card-holder-name">Name on Card</label>
        <div class="col-sm-9">
            {!! Form::text(null, null, [

                'class'                         => 'form-control',
                'required'                      => 'required',
                'data-stripe'                   => 'name',
                'data-parsley-required-message' => 'Name is required',
                'data-parsley-trigger'          => 'change focusout',
                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                'data-parsley-minlength'        => '2',
                'data-parsley-maxlength'        => '32',
                'data-parsley-class-handler'    => '#first-name-group',

            ]) !!}
        </div>
    </div>

{{--// 4242424242424242--}}
    {{--CARD NUMBER--}}
    <div class="form-group">
        <label class="col-sm-3 control-label" for="card-number">Card Number</label>
        <div class="col-sm-9">
            {!! Form::text(null, null, [
            'class'             => 'form-control',
            'data-stripe'       =>'number',
            'data-parsley-type' =>'number',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-required-message' => '16-digit credit card number required.',
            'data-parsley-max'              => '16',
            'required' ]) !!}
        </div>
    </div>


    {{--EXPIRATION DATE--}}
    <div class="form-group">
        <label class="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
        <div class="col-sm-9">
            <div class="row">

                {{--EXP MONTH--}}
                <div class="col-xs-3">
                    {!! Form::selectMonth(null, null, ['class' => 'form-control', 'data-stripe' => 'exp_month', 'required'], '%m') !!}
                </div>

                {{--EXP YEAR--}}
                <div class="col-xs-3">
                    {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, ['class' => 'form-control', 'data-stripe' => 'exp_year', 'required' ]) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="cvc">CVC</label>
        <div class="col-sm-3">
            {!! Form::text(null, null, [

                        'class'                         => 'form-control',
                        'data-stripe'                   =>'cvc',
                        'data-parsley-type'             =>'number',
                        'data-parsley-trigger'          => 'change focusout',
                        'data-parsley-required-message' => 'Only numbers allowed',
                        'required'
                        ]) !!}
        </div>
    </div>

</fieldset>

{{ Form::submit('Add Card', ['class'=>'btn btn-primary', 'id'=>'submitBtn']) }}
<button class="btn btn-default" data-dismiss="modal" class="close" >Cancel</button>

{!! Form::close() !!}
