@if($units)

<h1>Stuff here</h1>
<table class="table table-condensed">
    <thead>
    <th>Model</th>
    <th>Serial</th>
    <th>Part Number</th>
    <th>Condition</th>
    <th>Days Remaining</th>
    <th>Auto-renew</th>
    <th></th>
    </thead>

    @foreach($units as $unit)
        <?php
                $type = $unit->post_type;

                ?>



        <tr
        @if($unit->status=='expired')
            class="danger"

            @endif


        >
            <td>
                {{ $unit->model}} -  {{ var_dump($type) }}
            </td>
            <td>
                {{ $unit->serial }}
            </td>
            <td>
                {{ $unit->part_number }}
            </td>
            <td>
                {{ $unit->condition }}
            </td>
            <td>
                {{ $unit->days_to_go }}
            </td>
            <td>
                @if($unit->auto_renew == true)

                    <p class="text-center"><i class="fa fa-check" aria-hidden="true"></i></p>
                    @else


                @endif
            </td>

            <td>

                <a href="{{ route(''.$type.'s.edit', $unit->id) }}" class="btn btn-default btn-xs">Edit</a>
            </td>
        </tr>
    @endforeach
</table>

    @endif