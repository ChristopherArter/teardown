


    <div class="col-sm-3 well">

      <input type="hidden" id="STRIPE_KEY" value = "{{ env('STRIPE_KEY') }}"/>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#td_add_card">
            Add Card
        </button>

        <!-- Modal -->
        <div class="modal fade" id="td_add_card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add New Card</h4>
                    </div>
                    <div class="modal-body">
                    @include('includes.dashboard.partials._billingccform')
                    </div>

                </div>
            </div>
        </div>



        <p></p>


    </div>

    <div class="col-sm-9">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach($customer->sources->data as $item)


                <div class="panel

                @if($customer->default_source == $item->id)
                        panel-primary
                        @else
                        panel-default
                        @endif" data-stripe-card-id = {{$item->id }}>
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">

                                Billing Information for
                                <?php
                                // Display the card icon. Too bad there are no switch statements in Blade :(
                                switch ($item->brand) {

                                    case "Visa":
                                        echo '<i class="fa fa-cc-visa" aria-hidden="true"></i>';
                                        break;

                                    case "MasterCard":
                                        echo '<i class="fa fa-cc-mastercard" aria-hidden="true"></i>';
                                        break;

                                    case "American Express":
                                        echo '<i class="fa fa-cc-amex" aria-hidden="true"></i>';
                                        break;

                                    case "Discover":
                                        echo '<i class="fa fa-cc-discover" aria-hidden="true"></i>';
                                        break;

                                    case "JCB":
                                        echo '<i class="fa fa-cc-jcb" aria-hidden="true"></i>';
                                        break;

                                    case "Diners Club":
                                        echo '<i class="fa fa-cc-diners-club" aria-hidden="true"></i>';
                                        break;

                                    case "Unknown":
                                        echo '<i class="fa fa-credit-card-alt" aria-hidden="true"></i>';
                                        break;


                                }
                                ?>  ({{ $item->last4 }})


                            <?php

                            if ($customer->default_source == $item->id){
                            ?>
                            <span class="label label-default">Default</span>
                            <?php
                            }

                            ?>
                        </h4>
                    </div>

                        <div class="panel-body">

                            <dl class="dl-horizontal">
                                <dt>Name on Card</dt>
                                <dd>{{ $item->name }}</dd>
                                <p></p>
                                <dt>Card Number</dt>
                                <dd>**** **** **** {{ $item->last4 }}</dd>
                                <p></p>
                                <dt>Expiration</dt>
                                <dd>{{ $item->exp_month }}/{{ $item->exp_year }}</dd>

                            </dl>
                            <p></p>
                            <p></p>

<div class="row">
    @if($customer->default_source !== $item->id)
    <div class="col-xs-2">
        {!! Form::open([ 'url' => route('dashboard.newDefault'), 'method'=>'post', ] ) !!}
        {{ Form::hidden('newDefault', $item->id) }}

        {{ Form::submit('Set as Default', ['class'=>'btn btn-default btn-sm']) }}
        {!! Form::close() !!}
    </div>
    @endif

    <div class="col-xs-2">

        {!! Form::open([ 'url' => route('dashboard.deleteCard'), 'method'=>'post', ] ) !!}
        {{ Form::hidden('deleteID', $item->id) }}

        {{ Form::submit('Remove Card', ['class'=>'btn btn-default btn-sm']) }}
        {!! Form::close() !!}
    </div>
</div>




                        </div>

                </div>

            @endforeach


        </div>
    </div>
