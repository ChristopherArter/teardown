
@section('scripts')

    {{ Html::script('/src/js/vendor/parsley.js') }}
    <script type="text/javascript">
        $('.td-dash-cc-form').parsley();
    </script>

@endsection
{{--FORM START--}}
{!! Form::open([

        'url' => '',
        'class'=>'td-dash-cc-form form-horizontal',
        'method'=>'post',
        'data-card-id'=>  '',
        'data-parsley-validate' ]


) !!}

<fieldset>
    <legend>Payment</legend>


    {{--NAME ON CARD--}}
    <div class="form-group" id="">
        <label class="col-sm-3 control-label" for="card-holder-name">Name on Card</label>
        <div class="col-sm-9">
            {!! Form::text('first_name', null, [

                'class'                         => 'form-control',
                'required'                      => 'required',
                'data-parsley-required-message' => 'First name is required',
                'data-parsley-trigger'          => 'change focusout',
                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                'data-parsley-minlength'        => '2',
                'data-parsley-maxlength'        => '32',
                'data-parsley-class-handler'    => '#first-name-group',

            ]) !!}
        </div>
    </div>


    {{--CARD NUMBER--}}
    <div class="form-group">
        <label class="col-sm-3 control-label" for="card-number">Card Number</label>
        <div class="col-sm-9">
            {!! Form::text(null, null, [
            'class'             => 'form-control',
            'data-stripe'       =>'number',
            'data-parsley-type' =>'number',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-required-message' => '16-digit credit card number required.',
            'required' ]) !!}
        </div>
    </div>


    {{--EXPIRATION DATE--}}
    <div class="form-group">
        <label class="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
        <div class="col-sm-9">
            <div class="row">

                {{--EXP MONTH--}}
                <div class="col-xs-3">
                    {!! Form::selectMonth(null, null, ['class' => 'form-control', 'data-stripe' => 'exp_month', 'required'], '%m') !!}
                </div>

                {{--EXP YEAR--}}
                <div class="col-xs-3">
                    {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, ['class' => 'form-control', 'data-stripe' => 'exp_year', 'required' ]) !!}
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-3 control-label" for="cvc">CVC</label>
        <div class="col-sm-3">
{!! Form::text(null, null, [

            'class'                         => 'form-control',
            'data-stripe'                   =>'number',
            'data-parsley-type'             =>'number',
            'data-parsley-required-message' => 'Only numbers allowed',
            'required',

            ]) !!}
        </div>
    </div>

    {!! Form::submit('Submit') !!}

</fieldset>


{!! Form::close() !!}
