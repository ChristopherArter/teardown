<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">




        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            {{--LOGO--}}
            <a href="{{ route('home') }}"><img class="td-aero-nav-logo" src="/src/images/teardown-aero-logo.svg" alt="Teardown.AERO" onerror="this.src='/src/images/teardown-aero-logo.png'; this.onerror=null;" /></a>

        </div>



        {{--SEARCH BAR--}}

        {!! Form::open([

                                   'url' => route('search', $term = null),
                                   'class'=>'navbar-form navbar-right',
                                   'method'=> 'GET',
                                   'role'   => 'search'  ] ) !!}




            <div class="form-search search-only">
                <i class="search-icon glyphicon glyphicon-search"></i>

                {!! Form::text('term', null, ['class'=>'form-control search-query',
                 'placeholder'=>'Search PN, SN, Model',
                 'id'=>'top_search_bar',
                 'autocomplete'=>'off']) !!}

            </div>
            {!! Form::close() !!}
        {{--/SEARCH BAR--}}




        {{--SHOPPING CART BUTTON--}}
        @if (Auth::check())
            @if(Auth::user()->hasUnpublished())
                <a href="{{ route('paymentform') }}" style="margin-left:10px; margin-right:10px;" class="btn pull-right navbar-btn btn-default"><i class="fa fa-shopping-cart" aria-hidden="true"></i> ({{ Auth::user()->unpublishedCount() }}) </a>
                @endif
        @endif
        {{--/SHOPPING CART BUTTON--}}


        {{--NEW LISTING BUTTON GROUP--}}
        @if (Auth::check())
            <div class="dropdown pull-right">
                <button class="btn navbar-btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-plus" aria-hidden="true"></i> New Listing
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                    <li><a href="{{ route('engines.create') }}">Engine</a></li>
                    <li><a href="{{ route('apus.create') }}">Auxiliary Power Unit</a></li>
                    @if(Auth::user()->canPostAircraft())
<li><a href="{{ route('aircraft.create') }}">Aircraft Teardown</a></li>
                    @endif
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Market Post</a></li>
                </ul>
            </div>
        @endif
                {{--/NEW LISTING BUTTON GROUP--}}








                {{--GENERAL NAV--}}



        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav pull-right">
                <li>
                    <a href="{{ route('aircraft.index') }}">Aircraft</a>
                </li>
                <li>
                    <a href="{{ route('engines.index') }}">Engines</a>
                </li>
                <li>
                    <a href="{{ route('apus.index') }}">APUs</a>
                </li>
                <li>
                    <a href="{{ route('market.index') }}">Market</a>
                </li>

                @if (Auth::check())

                    <!-- LOGGED IN USER DROP DOWN -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>

                            @if(Auth::user()->avatar)

                            <img id="top_user_avatar" src="{{ Auth::user()->avatar() }}"/>

                                @else

                                <i class="fa fa-user-circle-o" aria-hidden="true"></i>

                        @endif
                        {{ Auth::user()->name }}</b>
                        <?php if (Auth::user()->unreadMessageCount()){
                        ?>

                        <span style="margin-left:5px;" class="label label-success">{{ Auth::user()->unreadMessageCount() }}</span>
                        <?php
                        }

                        ?>


                    </a>
                    <ul id="login-dp" class="dropdown-menu">
                        <li>


                        </li>
                        <li>
                            <a href="{{ route('dashboard.profile') }}"><i class="fa fa-user" aria-hidden="true"></i> Dashboard</a>
                            <a href="{{ route('dashboard.messages') }}"><i class="fa fa-bell" aria-hidden="true"></i> Notifications

                            <?php if (Auth::user()->unreadMessageCount()){
                            ?>

                            <span class="label label-success">{{ Auth::user()->unreadMessageCount() }}</span>
                            <?php
                            }

                            ?>


                            </a>
                            <a href="{{ route('dashboard.listings') }}"><i class="fa fa-thumb-tack" aria-hidden="true"></i> My Listings</a>
                            <a href="#"><i class="fa fa-address-book" aria-hidden="true"></i> Membership</a>
                            <a href="{{ route('dashboard.invoices') }}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                            <a href="{{ route('dashboard.billing') }}"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Payment Options</a>
                            <a href=" {{ route('logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>

                        </li>
                    </ul>
                </li>

                @else
<li>

    <form method = "GET" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-small navbar-btn btn-primary">Login / Register</button>
    </form>

   </li>


                @endif


</ul>




        </div>

                {{--/ GENERAL NAV--}}
        <!-- /.navbar-collapse -->

    </div>

    <!-- /.container -->


</nav>