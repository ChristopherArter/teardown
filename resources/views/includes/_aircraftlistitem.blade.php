

<div class="media">
  <div class="media-left">
    <a href="{{ route('aircraft.show', $aircraft->id) }}">
      <img class="media-object thumbnail" style="width:64px;" src="{{ $avatar }}" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading"><a href="{{ route('aircraft.show', $aircraft->id) }}">{{ $aircraft->make }} {{ $aircraft->model }} | Ex-{{$aircraft->ex_airline }} </a></h4>
    <p><small>Listed by <strong>TAG Aero</strong> - Tail Number <strong>{{ $aircraft->tail_number }}</strong> - MSN <strong>{{ $aircraft->serial }}</strong></small><p>




  </div>
</div>