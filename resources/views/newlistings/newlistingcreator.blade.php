@extends('layouts.master')


@section('content')
<div class="container">
    <div class="row">

        <div class="col-sm-3 well">
            @if($status == 'draft')
                @include('newlistings.includes._newlistingseditsidebar')

                @elseif($status == 'publish')

                @include('newlistings.includes._editlistingssidebar', ['listing' => $listing])

            @endif


        </div>

        <div class="col-sm-8 col-sm-offset-1">

            @if($type == 'apu')
                @if($form == 'create')
                        @include('newlistings.includes._apucreateform')
                    @elseif($form == 'edit')
                        @include('newlistings.includes._apueditform')
                    @endif
            @endif

                @if($type == 'engine')
                    @include('newlistings.includes._engineform')
                @endif
        </div>




    </div>
</div>

    @endsection
@section('scripts')
    {{ Html::script('src/js/newListings.js') }}
    {{ Html::script('src/js/vendor/parsley.js') }}
    {{ Html::script('src/js/vendor/jquery.vide.min.js') }}
@endsection