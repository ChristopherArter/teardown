
<p class="text-center"><strong>Listings Cart</strong></p>
<p></p>


        {{--If user has Engines to list--}}


        @if(!$engines->isEmpty())
            <legend><small>Engines</small></legend>
            <ul class="list-unstyled">
                @foreach($engines as $engine)

                    <li style="margin-bottom:20px;">
                        <div class="row">
                            <div class="col-xs-8">{{ $engine->model }}{{ $engine->minor_variant }} <p><small>SN <strong> {{ $engine->serial }}</strong> - PN <strong>{{ $engine->part_number }}</strong></small></p>
                                <p style="margin-top:-10px;"><small>
                                        {{ Form::open(array('route' => array('engines.destroy', $engine->id), 'method' => 'delete')) }}
                                        <button class="btn btn-xs btn-default"  type="submit">Delete Listing</button>
                                        {{ Form::close() }}



                                    </small></p>

                            </div>
                            <div class="col-xs-4"><a href="{{ route('engines.edit',$engine->id) }}" class="pull-right btn btn-xs btn-default">Edit</a></div></div></li>


                @endforeach
            </ul>
        @endif
<p></p>


        {{--If user has APUs to list--}}
        @if(!$apus->isEmpty())
            <legend><small>APUS</small></legend>
            <ul class="list-unstyled">
                @foreach($apus as $apu)

                    <li style="margin-bottom:20px;">
                        <div class="row">




                            <div class="col-xs-8">{{ $apu->model }} <p><small>SN <strong> {{ $apu->serial }}</strong> - PN <strong>{{ $apu->part_number }}</strong></small></p>
                            <p style="margin-top:-10px;"><small>
                                    {{ Form::open(array('route' => array('apus.destroy', $apu->id), 'method' => 'delete')) }}
                                    <button class="btn btn-xs btn-default"  type="submit">Delete Listing</button>
                                    {{ Form::close() }}



                                </small></p></div>
                            <div class="col-xs-4"><a href="{{ route('apus.edit',$apu->id) }}" class="pull-right btn btn-xs btn-default">Edit</a>
                                </div></div></li>


                @endforeach
            </ul>
        @endif





    <div class="row"></div>
    <div class="col-xs-12">
        @if($user->hasUnpublished())

           {{--make this a dropdown button--}}
            <div class="dropdown">
                <button class="btn navbar-btn btn-default btn-block dropdown-toggle" type="button" id="add_another_sidebar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add Another
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="add_another_sidebar">

                    <li><a href="{{ route('engines.create') }}">Engine</a></li>
                    <li><a href="{{ route('apus.create') }}">Auxiliary Power Unit</a></li>
                    <li><a href="#">Aircraft Teardown</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Market Post</a></li>
                </ul>
            </div>
            @else
            <p class="text-center">No listings saved yet.</p>
            @endif

        @if($user->hasUnpublished())  <a class="btn  btn-primary btn-block" href="{{ route('paymentform') }}">Checkout & Publish</a>@endif


    </div>
