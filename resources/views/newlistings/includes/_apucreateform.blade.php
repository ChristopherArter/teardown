
<legend>Create New Listing - {{ $type }}</legend>
{{--CREATE APU FORM--}}
<form class="apu-form"  enctype="multipart/form-data" method="POST" action="/apus">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">


    {{--Error Display--}}
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i><strong> Whoops!</strong> There were some errors with your submission. See details below.
            <hr>
          <?php  foreach ($errors->all() as $message) {
   echo '<p>'.$message.'</p>';
} ?>
        </div>
        @endif
        <div class="row">

            {{--Part Number--}}
            <div class="part-number col-sm-6">
                <div class="form-group @if ($errors->has('part_number')) has-error @endif">
                    <label for="part_number">Part Number</label>
                    <input type="text" class="form-control" name="part_number" value="{{ old('part_number') }}" >
                </div>
            </div>

            {{--Serial Number--}}
            <div class="serial col-sm-6">
                <div class="form-group @if ($errors->has('serial')) has-error @endif">
                    <label for="serial_number">Serial Number</label>
                    <input type="text" class="form-control" name="serial" value="{{ old('serial') }}" >
                </div>
            </div>

            {{--Model Number--}}
            <div class="model col-sm-6">
                <div class="form-group @if ($errors->has('model')) has-error @endif">
                    <label for="model_number ">Model Number</label>
                    <input type="text" class="form-control" name="model" value="{{ old('model') }}">
                </div>
            </div>

            {{--Condition--}}
            <div class="col-sm-6">

                <div class="form-group @if ($errors->has('condition')) has-error @endif">
                <label for="condition">Condition</label>
                <select class="form-control" name="condition" id="condition">

                    @if(!old('condition'))
                    <option value="" selected>Select</option>
                    @else
                        <option value="">Select</option>
                    @endif

                        {{--AS REMOVED--}}
                        @if(old('condition') == 'AR' )
                            <option value="AR" selected>As-Removed</option>
                            @else
                            <option value="AR">As-Removed</option>
                            @endif

                        {{--SV--}}
                        @if(old('condition') == 'SV' )
                            <option value="SV" selected>Serviceable</option>
                        @else
                            <option value="SV">Serviceable</option>
                        @endif

                        {{--OH--}}
                        @if(old('condition') == 'OH' )
                            <option value="OH" selected>Overhauled</option>
                        @else
                            <option value="OH">Overhauled</option>
                        @endif

                        {{--NE--}}
                        @if(old('condition') == 'NE' )
                            <option value="NE" selected>New</option>
                        @else
                            <option value="NE">New</option>
                        @endif
                </select>
                    </div>
            </div>



            {{--8130--}}

            <div class="8130-details well col-sm-12">
                <legend><small>Tag Details</small></legend>
                     <div class="form-group @if ($errors->has('tagged_by')) has-error @endif">

                         <label for="8130_shop_name">Tagged By</label>
                         <input @if(old('condition') == 'SV' or old('condition') == 'OH' or old('condition') == 'NE') value="{{ old('tagged_by') }}" @endif type="text" class="8130 form-control" name="tagged_by"  @if (old('condition') == 'SV' or old('condition') == 'OH' or old('condition') == 'NE') disabled="false" @else disabled="true" @endif>
                     </div>



                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group @if ($errors->has('details')) has-error @endif">
                        <label>8130 Block Details</label>
                        <textarea name="details" class="8130 8130-details form-control" disabled="true">{{ old('details') }}</textarea>
                            </div>
                    </div>

                        {{--TAG DATE--}}
                    <div class="col-sm-4">
                        <label>Tag Date</label>
                        <div class="form-group">
                            {{--TAG MONTH--}}
                        <div class="col-xs-4">
                            <div class="form-group @if ($errors->has('tag_month')) has-error @endif">

                            <input type="text" class="8130 form-control" name="tag_month"  disabled="true" value="{{ old('tag_month') }}">
                            <span id="helpBlock" class="text-center help-block"><small>Month</small></span>
                                </div>
                        </div>
                            {{--TAG DAY--}}
                        <div class="col-xs-4">
                            <div class="form-group @if ($errors->has('tag_day')) has-error @endif">
                            <input type="text" class="8130 form-control" name="tag_day"  disabled="true" value="{{ old('tag_day') }}">
                            <span id="helpBlock" class="text-center help-block"><small>Day</small></span>
                                </div>
                        </div>

                            {{--TAG YEAR--}}
                        <div class="col-xs-4">
                            <div class="form-group @if ($errors->has('tag_day')) has-error @endif">
                            <input type="text" class="8130 form-control" name="tagged_year"  disabled="true" value="{{ old('tag_year') }}">
                            <span id="helpBlock" class="text-center help-block"><small>Year</small></span>
                        </div>
                            </div>
                            </div>



                    </div>
                </div>




            </div>

        {{--===== TIMES & CYCLES ======--}}
<legend><small>Times & Cycles</small></legend>
            {{--TSN--}}
            <div class="tsn col-sm-3">
                <div class="form-group">
                    <label for="tsn">TSN</label>
                    <input autocomplete="off" type="text" class="form-control" name="tsn" value="{{ old('tsn') }}">
                    <span id="helpBlock" class="help-block"><small>Time Since New</small></span>
                </div>
            </div>

            {{--CSN--}}
            <div class="tsn col-sm-3">
                <div class="form-group">
                    <label for="csn">CSN</label>
                    <input autocomplete="off" type="text" class="form-control" name="csn" value="{{ old('csn') }}" >
                    <span id="helpBlock" class="help-block"><small>Cycles Since New</small></span>
                </div>
            </div>

            {{--TSLSV--}}
            <div class="tsn col-sm-3">
                <div class="form-group">
                    <label for="tslsv">TSLSV</label>
                    <input  autocomplete="off" type="text" class="form-control" name="tslsv" value="{{ old('tslsv') }}">
                    <span id="helpBlock" class="help-block"><small>Time Since Last Shop Visit</small></span>
                </div>

            </div>

            {{--CSLSV--}}
            <div class="tsn col-sm-3">
                <div class="form-group">
                    <label for="cslsv">CSLSV</label>
                    <input autocomplete="off" type="text" class="form-control" name="cslsv"value="{{ old('cslsv') }}" >
                    <span id="helpBlock" class="help-block"><small>Cycles Since Last Shop Visit</small></span>
                </div>



            </div>


            <legend><small>Trace & Terms</small></legend>

                {{--Trace--}}
                <div class="part-number col-sm-3">
                    <div class="form-group">
                        <label for="trace">Trace</label>
                        <select class="form-control" name="trace" id="trace">


                            @if(!old('condition'))
                                <option selected>Select</option>
                            @else
                                <option>Select</option>
                            @endif

                            {{--121--}}
                            @if(old('trace') == '121' )
                                <option value="121" selected>121 Operator</option>
                            @else
                                <option value="121">121 Operator</option>
                            @endif

                            {{--129--}}
                            @if(old('trace') == '129' )
                                <option value="129" selected>129 Operator</option>
                            @else
                                <option value="129">129 Operator</option>
                            @endif

                            {{--135--}}
                            @if(old('trace') == '135' )
                                <option value="135" selected>135 Operator</option>
                            @else
                                <option value="135">135 Operator</option>
                            @endif

                            {{--145--}}
                            @if(old('trace') == '145' )
                                <option value="145" selected>145 Repair Station</option>
                            @else
                                <option value="145">145 Repair Station</option>
                            @endif

                            {{--Foreign--}}
                            @if(old('trace') == 'Foreign' )
                                <option value="Foreign" selected>Foreign Operator</option>
                            @else
                                <option value="Foreign">Foreign Operator</option>
                            @endif

                            {{--NA--}}
                            @if(old('trace') == 'NA' )
                                <option value="NA" selected>NA</option>
                            @else
                                <option value="NA">NA</option>
                            @endif

                        </select>
                    </div>
                </div>

            <div class="col-sm-3">

                <label for="tslsv">Minipack URL</label>

                <input autocomplete="off" type="text" class="form-control" name="minipack" value="{{ old('minipack') }}">
                <span class="help-block"><small><i class="fa fa-dropbox" aria-hidden="true"></i> DropBox URL (or other)</small></span>



            </div>

                {{--Terms--}}
                <div class="serial col-sm-5">
                    <div class="form-group">
                        <label>Terms</label>
                        <p></p>
                        <label class="checkbox-inline">

                            <input type="checkbox" name="outright" value="outright" @if(old('outright')) checked @endif > Outright Sale
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="exchange" value="exchange" @if(old('exchange')) checked @endif > Exchange
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="lease" value="lease" @if(old('lease')) checked @endif > Lease
                        </label>
                    </div>

            </div>




        </div>


        <div class="row">
            <div class="sale-terms col-sm-12">
                <div class="form-group">
                    <textarea class="form-control" readonly="readonly">{!! config('app.TOS') !!}</textarea>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="agreement" name="agreement"> I agree to Teardown Aero LLC Terms of Service.
                    </label>

                </div>

            </div>

        </div>





    <button class="btn btn-primary" id="new-apu-submit"  type="submit">Done</button>


</form>