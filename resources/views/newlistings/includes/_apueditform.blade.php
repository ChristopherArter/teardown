@if($method == 'post')
    <legend>Create New APU</legend>
    @else
    <legend>Edit Listing - {{ $editApu->model }}</legend>
    @endif


<?php



?>







{!! Form::open([

        'url' => 'apus/' . $editApu->id,
        'class'=>'listing-form',
        'data-parsley-validate',
        'method'=>$method]


) !!}

<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
    @endforeach
</div> <!-- end .flash-message -->
{{ Form::model($Apu, ['route' => ['apus.update', $editApu->id]]) }}
{{--EDIT APU FORM--}}


    {{--Error Display--}}
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i><strong> Whoops!</strong> There were some errors with your submission. See details below.
            <hr>
          <?php  foreach ($errors->all() as $message) {
   echo '<p>'.$message.'</p>';
} ?>
        </div>
        @endif
        <div class="row">


            <?php

            if($editApu->status == 'publish'){

                $disabled = 'readonly="readonly"';

            } else {

                $disabled = '';
            } ?>


            {{--Part Number--}}
            <div class="part-number col-sm-6">
                <div class="form-group @if ($errors->has('part_number')) has-error @endif">
                    <label for="part_number">Part Number</label>

                    {{ Form::text('part_number', $editApu->part_number, ['class'=>'form-control', $disabled]) }}


                </div>
            </div>

            {{--Serial Number--}}
            <div class="serial col-sm-6">
                <div class="form-group @if ($errors->has('serial')) has-error @endif">
                    <label for="serial_number">Serial Number</label>
                    {{ Form::text('serial', $editApu->serial, ['class'=>'form-control', $disabled]) }}
                </div>
            </div>

            {{--Model Number--}}
            <div class="model col-sm-6">
                <div class="form-group @if ($errors->has('model')) has-error @endif">
                    <label for="model_number ">Model Number</label>
                    {{ Form::text('model', $editApu->model, ['class'=>'form-control', $disabled]) }}
                </div>
            </div>

            {{--Condition--}}
            <div class="col-sm-6">

                <div class="form-group @if ($errors->has('condition')) has-error @endif">
                <label for="condition">Condition</label>


                    {{ Form::select('condition',

                    ['AR' => 'As-Removed',
                     'SV' => 'Serviceable',
                     'OH'=>'Overhauled',
                     'NE'=>'New' ], null, ['placeholder' => 'Select condition', 'class'=>'form-control', 'id'=>'condition']) }}


                    </div>
            </div>

        <?php
            if($editApu->condition == 'SV' || $editApu->condition == 'OH' || $editApu->condition == 'NE'){

                $disabled = false;

            } else {

                $disabled = true;
            }

             ?>
            {{--8130--}}

            <div class="8130-details well col-sm-12">
                <legend><small>Tag Details</small></legend>
                     <div class="form-group @if ($errors->has('tagged_by')) has-error @endif">

                         <label for="8130_shop_name">Tagged By</label>




                         {{ Form::text('tagged_by', $editApu->tagged_by, ['class'=>'form-control 8130', 'disabled'=>$disabled ]) }}

                     </div>



                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group @if ($errors->has('details')) has-error @endif">
                        <label>8130 Block Details</label>


                       {{ Form::textarea('details', $editApu->details, ['class'=>'form-control 8130', 'size'=>'30x3', 'disabled'=>$disabled ]) }}

                            </div>
                    </div>

                        {{--TAG DATE--}}
                    <div class="col-sm-4">
                        <label>Tag Date</label>
                        <div class="form-group @if ($errors->has('tag_date')) has-error @endif">

            {{ Form::date('tag_date', $editApu->tag_date, ['class'=>'form-control 8130', 'disabled'=>$disabled ]) }}
                            <span class="help-block">Ex: 01/31/1986</span>
                            </div>
                    </div>
                </div>
            </div>

        {{--===== TIMES & CYCLES ======--}}
<legend><small>Times & Cycles</small></legend>

            {{--TSN--}}
            <div class="tsn col-sm-3">
                <div class="form-group">
                    <label for="tsn">TSN</label>
                    {{ Form::text('tsn', $editApu->tsn, [


                    'class'=>'form-control', 'autocomplete'=>'off',
                    'data-parsley-type'=> 'integer',
                    'data-parsley-trigger'          => 'change focusout',


                    ]) }}
                    <span id="helpBlock" class="help-block"><small>Time Since New</small></span>
                </div>
            </div>

            {{--CSN--}}
            <div class="tsn col-sm-3">
                <div class="form-group">
                    <label for="csn">CSN</label>
                    {{ Form::text('csn', $editApu->csn, ['class'=>'form-control', 'autocomplete'=>'off',
                    'data-parsley-type'=> 'integer',
                    'data-parsley-trigger'          => 'change focusout']) }}
                    <span id="helpBlock" class="help-block"><small>Cycles Since New</small></span>
                </div>
            </div>

            {{--TSLSV--}}
            <div class="tsn col-sm-3">
                <div class="form-group">
                    <label for="tslsv">TSLSV</label>
                    {{ Form::text('tslsv', $editApu->tslsv, ['class'=>'form-control', 'autocomplete'=>'off',
                    'data-parsley-type'=> 'integer',
                    'data-parsley-trigger'          => 'change focusout']) }}
                    <span id="helpBlock" class="help-block"><small>Time Since Last Shop Visit</small></span>
                </div>

            </div>

            {{--CSLSV--}}
            <div class="tsn col-sm-3">
                <div class="form-group">
                    <label for="cslsv">CSLSV</label>
                    {{ Form::text('cslsv', $editApu->cslsv, ['class'=>'form-control', 'autocomplete'=>'off',
                    'data-parsley-type'=> 'integer',
                    'data-parsley-trigger'          => 'change focusout']) }}
                    <span id="helpBlock" class="help-block"><small>Cycles Since Last Shop Visit</small></span>
                </div>



            </div>


            <legend><small>Trace & Terms</small></legend>

                {{--Trace--}}
                <div class="part-number col-sm-2">
                    <div class="form-group">
                        <label for="trace">Trace</label>
                        {{ Form::select('trace',

            [

                '121' => '121 Operator',
                '129' => '129 Operator',
                '135'=>'135 Operator',
                '145'=>'145 Repair Station',
                 'OEM'=>'OEM',
                 'Foreign'=>'Foreign Operator',
                 'NA'=>'N/A'],
             null, ['placeholder' => 'Select', 'class'=>'form-control', 'id'=>'trace']) }}
                    </div>
                </div>

            <div class="col-sm-3">

                <label for="tslsv">Minipack URL</label>

                {{ Form::text('minipack', $editApu->minipack, ['class'=>'form-control', 'autocomplete'=>'off']) }}
                <span class="help-block"><small><i class="fa fa-dropbox" aria-hidden="true"></i> DropBox URL (or other)</small></span>



            </div>

                {{--Terms--}}
                <div class="serial col-sm-6">
                    <div class="form-group">
                        <label>Listing Type</label>
                        <p></p>
                        <label class="checkbox-inline">

                            {{ Form::checkbox('sale', '1') }} Sale
                        </label>
                        <label class="checkbox-inline">
                            {{ Form::checkbox('lease', '1') }} Lease
                        </label>
                        <label class="checkbox-inline">
                            {{ Form::checkbox('exchange', '1') }} Exchange
                        </label>
                        <label class="checkbox-inline">
                            {{ Form::checkbox('part_out', '1') }} Part-out
                        </label>
                    </div>

            </div>




        </div>


            {{--Additional comments--}}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                    <label>Additional Comments</label>
                        {{ Form::textarea('comments', $editApu->comments, ['class'=>'form-control', 'size'=>'30x3']) }}
                    </div>

                </div>
            </div>



<div class="checkbox">
    <label>{{ Form::checkbox('auto_renew', '1') }} Auto-renew this listing for <strong>${{ config('stripe.apu.price')/100 }}</strong> every 30 days.</label>
    <p class="help-block"><small>This will automatically renew the listing after 30 days until you cancel the listing. You may turn-off auto renewal at any time in your listings management area of your <a href="{{ route('dashboard', ['section'=>'listings']) }}">Dashboard.</a></small></p>
</div>
<div class="form-group">
    {{ Form::submit('Save', ['class'=>'btn btn-primary']) }}
</div>




{!! Form::close() !!}