<h3>{{ $listing->days_to_go }}</h3>
<p><strong>Days remaining until expiration.</strong></p>
<p>Expires {{ Carbon::now()->addDays($listing->days_to_go)->toFormattedDateString() }}</p>

<hr>
<p><strong>Actions:</strong></p>


<div class="btn-group btn-group-justified" role="group" aria-label="...">


    @if(!$listing->auto_renew)
    <div class="btn-group" role="group">
        <a class="btn btn-default" data-toggle="modal" data-target="#add30daysmodal">Add 30 Days</a>
    </div>
    @endif

    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default">Remove Listing</button>
    </div>





    {{--ADD 30 DAYS MODAL--}}
    @if(!$listing->auto_renew)
    <!-- Modal -->
    <div class="modal fade" id="add30daysmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            {!! Form::open(['url'=>'foo/bar']) !!}
            <div class="modal-content">

                <div class="modal-body">

                 <div class="col-xs-12">

                     @if($user->isPremium())
                         <h4>Would you like to add 30 more days to this listing?</h4>
                         If you confirm, your listing will remain visible until it expires.
                         @else

                         @if($user->hasStripeId())

                             put payment select here

                             @else

                             Please add a payment method in your dashboard.

                             @endif

                         @endif
                 </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

    @endif







</div>