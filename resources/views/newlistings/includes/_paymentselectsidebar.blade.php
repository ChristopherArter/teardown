<?php

// Access card information
    $defaultID = $customer->default_source;
    $default = $customer->sources->retrieve($defaultID);

    $sources = $customer->sources->all(array(
            'object' => 'card'));



?>


<div class="well">
    {!! Form::open([

            'url' => 'checkout/process',
            'class'=>'payment-form',
            'method'=>'post']


    ) !!}
<div class="form-group">
    <label>Payment Method:</label>
    <select name="card_id" class="form-control">
        <option value="{{ $default->id }}" selected>(default) {{ $default->brand }} - ({{ $default->last4 }})</option>

        @foreach($customer->sources->data as $source)
@if($source->id !== $default->id)

                <option value="{{ $source->id }}">{{ $source->brand }} - ({{ $source->last4 }})</option>

@endif
            @endforeach

    </select>
    <p></p>
    <p class="text-right"><small><a href="#">Manage Payment Options</a></small></p>

</div>

    <hr>
   <h3 class="text-center">${{ $total }}</h3>

    {{ Form::submit('Complete Purchase', ['class'=>'btn btn-primary btn-lg btn-block']) }}



    {!! Form::close() !!}
    <hr>
    <strong>Frequently Asked Questions</strong>
    <p></p>
    <p><strong>Q:</strong> When will my listings be live?</p>
    <p><strong>A:</strong> They will go live as soon as payment clears. Typically, this happens right away.</p>
    <p></p>
    <p><strong>Q:</strong> Will they be included in weekly emails?</p>
    <p><strong>A:</strong> Yes, they will be added to all regular emails, and alert emails.</p>


</div>
