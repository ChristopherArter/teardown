@extends('layouts.master')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('newlistings.includes._paymentconfirmationsidebar')
            </div>

            <div class="col-sm-8 col-sm-offset-1">
                <p class="alert alert-success">Listings successfully posted!</p>




                @if(!$engines->isEmpty())
                    @foreach($engines as $engine)
                        <h4>{{ $engine->model }}</h4>

                        <div class="row">
                            <div class="col-sm-9">
                                <p>
                                    <small>Serial No. <strong>{{ $engine->serial }}</strong> |  Part No. <strong>{{ $engine->part_number }}</strong></small>
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <a href="{{ route('engines.edit', $engine->id) }}" class="btn btn-xs btn-default pull-right">Edit</a>

                            </div>
                        </div>

                    @endforeach
                @endif









                @if(!$apus->isEmpty())
                @foreach($apus as $apu)
                    <h4>{{ $apu->model }}</h4>

                    <div class="row">
                        <div class="col-sm-9">
                            <p>
                                <small>Serial No. <strong>{{ $apu->serial }}</strong> |  Part No. <strong>{{ $apu->part_number }}</strong></small>
                            </p>
                        </div>
                        <div class="col-sm-3">
                            <a href="{{ route('apus.edit', $apu->id) }}" class="btn btn-xs btn-default pull-right">Edit</a>

                        </div>
                    </div>

                @endforeach
                @endif
                    <hr>

                <div class="row">

            

             {{--            Get the $invoiceId from one of the models above via relationship. But I can't get relationships working atm --}}


                      {{--   <h4>Download Invoice:</h4>
                        <a href="{{ route('dashboard.billing.download', $invoiceId ) }}" class="btn btn-primary">Download PDF</a> --}}


                </div>

                    </div>

            </div>
        </div>
