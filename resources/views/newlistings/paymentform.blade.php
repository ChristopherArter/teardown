@extends('layouts.master')


@section('content')

    <div class="container">




        <div class="row">
            <div class="col-sm-3">
          @include('newlistings.includes._paymentselectsidebar')
            </div>



        {{--APU LOOP--}}

        @if(!$apus->isEmpty())
        <div class="col-sm-8 col-sm-offset-1">
      

                {{--   Error  --}}

                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                     @if(isset($error))

                        {{ $error }}

                    @endif
                </div>

                           


    @foreach($apus as $apu)
    <h4>{{ $apu->model }}</h4>


    <div class="col-sm-9">





        <p>
            <small>Serial No. <strong>{{ $apu->serial }}</strong> |  Part No. <strong>{{ $apu->part_number }}</strong></small>
        </p>
    </div>
    <div class="col-sm-3">
        <a href="{{ route('apus.edit', $apu->id) }}" class="btn btn-xs btn-default pull-right">Edit</a>
        ${{ $apu->price/100 }}
    </div>

    <hr>
    @endforeach
            </div>
                @endif



            @if(!$engines->isEmpty())
                <div class="col-sm-8 col-sm-offset-1">
                    @foreach($engines as $engine)
                        <h4>{{ $engine->model }}</h4>

                        <div class="row">
                            <div class="col-sm-9">
                                <p>
                                    <small>Serial No. <strong>{{ $engine->serial }}</strong> |  Part No. <strong>{{ $engine->part_number }}</strong></small>
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <a href="{{ route('apus.edit', $engine->id) }}" class="btn btn-xs btn-default pull-right">Edit</a>
                                ${{ $engine->price/100 }}
                            </div>
                        </div>
                        <hr>
                    @endforeach
                </div>
            @endif




        </div>
    </div>



    @endsection