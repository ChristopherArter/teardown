@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row well">
            <div class="col-sm-4">
            <a class="btn btn-default btn-block">Aircraft Teardown</a>
            </div>
            <div class="col-sm-4">
                <a class="btn btn-default btn-block">Engine Listing</a>
            </div>
            <div class="col-sm-4">
                <a href="{{ route('apus.create') }}" class="btn btn-default btn-block">Auxiliary Power Unit</a>
            </div>
        </div>
    </div>




    @endsection