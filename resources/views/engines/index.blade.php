@extends('layouts.master')
@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
@endsection
@section('title','Engines for sale, lease, and exchange.')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Engines</h1>

                <hr>

                <table id="listings" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Model</th>
                        <th>Variant</th>
                        <th>PN</th>
                        <th>ESN</th>
                        <th>Cond.</th>
                        <th>Trace</th>
                        <th>TSN</th>
                        <th>CSN</th>
                        <th>TSLSV</th>
                        <th>CSLSV</th>
                        <th>Controls</th>
                    </tr>
                    </thead>

                    <tbody>

                    @foreach($engines as $engine)
                        <tr>
                            <td>{{ $engine->model }}</td>
                            <td>{{ $engine->minor_variant }}
                            <td>{{ $engine->part_number }}</td>
                            <td>{{ $engine->serial }}</td>
                            <td>{{ $engine->condition }}</td>
                            <td>{{ $engine->trace }}</td>
                            <td>{{ $engine->tsn }}</td>
                            <td>{{ $engine->csn }}</td>
                            <td>{{ $engine->tslsv }}</td>
                            <td>{{ $engine->cslsv }}</td>
                            <td><a class ="btn btn-default btn-sm" href="{{ route('engines.show',$engine->id) }}">View</a>
                                <a class="btn btn-default btn-sm"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a class="btn btn-default btn-sm"><i class="fa fa-envelope-o" aria-hidden="true"></i></i></a>
                            </td>

                        </tr>
                    @endforeach


                    </tbody>
                </table>

            </div>

        </div>
    </div>
@endsection

