@extends('layouts.master')


@section('content')

    <div class="container">
<div class="row">
    <div class="col-xs-12">
        <h3>Search for <strong>"{{ $term }}"</strong></h3>
    </div>
</div>


        {{--APU ROW--}}
        @if(!$apus->isEmpty())
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                <div class="panel-heading"><strong>Auxiliary Power Units</strong></div>
                    <div class="panel-body">


                    <table class="table table-condensed">
                        <thead>
                        <th>Model</th>
                        <th>Serial</th>
                        <th>Part Number</th>
                        <th>Cond</th>
                        </thead>
                        <tbody>
                        @foreach($apus as $apu)
                        <tr>
                            <td>{{ $apu->model }}</td>
                            <td>{{ $apu->serial }}</td>
                            <td>{{ $apu->part_number }}</td>
                            <td>{{ $apu->condition }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                        </div>
                    </div>
            </div>
        </div>
            @endif




        {{--ENGINE ROW--}}
        @if(!$engines->isEmpty())
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Engines</strong></div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <thead>
                                <th>Model</th>

                                <th>Serial</th>
                                <th>Part Number</th>
                                <th>Cond</th>
                                </thead>
                                <tbody>
                                @foreach($engines as $engine)
                                    <tr>
                                        <td>{{ $engine->model }}{{ $engine->minor_variant }}</td>
                                        <td>{{ $engine->serial }}</td>
                                        <td>{{ $engine->part_number }}</td>
                                        <td>{{ $engine->condition }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>

                </div>
            </div>
        @endif

        {{--AIRCRAFT--}}
        @if(!$aircrafts->isEmpty())
            <div class="row">
                <div class="col-xs-12">
                    <h3>Aircraft</h3>
                    @foreach($aircrafts as $aircraft)

                        <div class="panel">
                            <div class="panel-body">
                            <p><strong>{{ $aircraft->model }}</strong></p>
                            <p><strong>{{ $aircraft->serial }}</strong></p>
                            <p><strong>{{ $aircraft->tail_number }}</strong></p>
                        </div>
                        </div>

                    @endforeach
                </div>
            </div>
        @endif



                {{--AIRCRAFT--}}
                @if(!$marketPosts->isEmpty())
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Market Posts</h3>
                            @foreach($marketPosts as $post)

                                <div class="panel panel-default">
                                    <div class="panel-heading">{{ $post->market_title }}</div>
                                    <div class="panel-body">
                                        <p>{{ $post->market_body }}</p>
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    </div>
                @endif





    </div>


    @endsection