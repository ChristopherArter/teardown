@extends('mail.mastermail')



@section('content')

    <h1>Thank you!</h1>
    <h3>Your listings are now live!</h3>
    <p>Visit your dashboard at any time to update, renew, or delete your listings. Your invoice is ready for download below.</p>
    <br>





    @if(!$apuListings->isEmpty())

        @foreach($apuListings as $listing)

            <hr style="border: 1px solid #ddd; ">
            <p>Model:<strong> {{ $listing->model }}</strong>  SN: <strong>{{ $listing->serial }}</strong> PN: <strong>{{ $listing->part_number }}</strong> </strong></p>
            <p style="margin-top:-5px;"><a href="{{ url('apus/'.$listing->id.'/edit') }}">Manage Listing</a></p>

        @endforeach

    @endif



    @if(!$engineListings->isEmpty())


        @foreach($engineListings as $listing)

            <hr style="border: 1px solid #ddd; ">
            <p>Model:<strong> {{ $listing->model }}{{ $listing->minor_variant }}</strong>  ESN: <strong>{{ $listing->serial }}</strong> PN: <strong>{{ $listing->part_number }}</strong> </strong></p>
            <p style="margin-top:-5px;"><a href="{{ url('engines/'.$listing->id.'/edit') }}">Manage Listing</a></p>

        @endforeach

    @endif









    <table border="0" cellpadding="0" cellspacing="0" width="200px" style="background-color:#F4D03F; border:1px solid #dab10c; border-radius:5px;">
        <tr>
            <td align="center" valign="middle" style="color:#222222;font-size:16px; font-weight:bold; letter-spacing:-.5px; line-height:150%; padding-top:15px; padding-right:30px; padding-bottom:15px; padding-left:30px;">
                <a href="{{ route('dashboard.billing.download', $invoice->id) }}" target="_blank" style="color:#FFFFFF; text-decoration:none;">Download Invoice</a>
            </td>
        </tr>
    </table>

@endsection