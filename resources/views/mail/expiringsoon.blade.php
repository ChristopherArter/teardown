@extends('mail.mastermail')


@section('content')

    <h1>Uh oh!</h1>
    <h3>Your listings are about to expire!</h3>

    <p>The listings below are set to expire soon. If you would like to renew them, <a href="{{ route('login') }}">log-in</a> and go to your <a href="{{ route('dashboard.listings') }}">listings</a> section to renew, update, or remove this listing.</p>


    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>


                @if(!$apuListings->isEmpty())

                @foreach($apuListings as $listing)

                    <hr style="border: 1px solid #ddd; ">
                <p>Model:<strong> {{ $listing->model }}</strong>  SN: <strong>{{ $listing->serial }}</strong> PN: <strong>{{ $listing->part_number }}</strong> </strong></p>
                    <p style="margin-top:-5px;"><a href="{{ url('apus/'.$listing->id.'/edit') }}">Manage Listing</a></p>

                    @endforeach

                @endif



                @if(!$engineListings->isEmpty())


                    @foreach($engineListings as $listing)

                        <hr style="border: 1px solid #ddd; ">
                        <p>Model:<strong> {{ $listing->model }}{{ $listing->minor_variant }}</strong>  ESN: <strong>{{ $listing->serial }}</strong> PN: <strong>{{ $listing->part_number }}</strong> </strong></p>
                        <p style="margin-top:-5px;"><a href="{{ url('engines/'.$listing->id.'/edit') }}">Manage Listing</a></p>

                    @endforeach

                    @endif
            </td>
        </tr>
    </table>

@endsection