@extends('mail.mastermail')


@section('content')

    <h2 style="text-align: center;">Aircraft Teardowns</h2>
    <p style="text-align: center;">This is your periodic email detailing aircraft currently in teardown, as well as Engines & APUs currently available. </p>




    @if(!$engines->isEmpty())

        <h3 style="border-bottom: 3px solid #ddd; padding-bottom:20px; text-align:center;">Engines</h3>
        <p style=" text-align:center;">Engines for sale, lease or exchange</p>

        @foreach($engines as $engine)
            <span><a
                        style="text-decoration: none;"

                        href="{{ route('engines.show', $engine->id ) }}"><strong>{{ $engine->model }}{{ $engine->minor_variant  }}</strong> - {{ $engine->serial }}</a></span>
            <p style="margin-top:-3px;padding-bottom:10px;">Condition: <strong style="text-transform: uppercase;">{{ $engine->condition }}</strong> PN: <strong style="text-transform: uppercase;">{{ $engine->condition }}</strong></p>

        @endforeach

    @endif







    @if(!$apus->isEmpty())

    <h3 style="border-bottom: 3px solid #ddd; padding-bottom:20px; text-align:center;">Auxiliary Power Units</h3>
    <p style=" text-align:center;">Today's APUs</p>

@foreach($apus as $apu)
    <span><a style="text-decoration: none;" href="{{ route('apus.show', $apu->id ) }}"><strong>{{ $apu->model }}</strong> - {{ $apu->serial }}</a></span>
    <p style=" margin-top:-3px; padding-bottom:10px;">Condition: <strong style="text-transform: uppercase;">{{ $apu->condition }}</strong> PN: <strong style="text-transform: uppercase;">{{ $apu->condition }}</strong></p>

    @endforeach

    @endif






    @if(!$marketPosts->isEmpty())

        <h3 style="border-bottom: 3px solid #ddd; padding-bottom:20px; text-align:center;">Latest from the Market</h3>

        @foreach($marketPosts as $post)
            <span><a style="text-decoration: none;" href="#"><strong>{{ $post->market_title }}</strong></a></span>
            <p style=" margin-top:-3px; padding-bottom:10px;">{{ $post->market_body }}</p>

        @endforeach

    @endif







    @endsection