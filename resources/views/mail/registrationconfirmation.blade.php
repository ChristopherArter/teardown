@extends('mail.mastermail')


@section('content')

<h1>Welcome!</h1>
    <br>

    <p>My name is Chris Arter, and I built Teardown.AERO for aviation professionals as a unified resource for all aircraft teardowns. I wanted to say thank you for creating an account, and I look forward to seeing how incredibly useful this tool will be to you and your company. If you enjoy this website, please, consider sharing it with your colleagues.</p>
    <br>
    <p>Enjoy!</p>

    @endsection