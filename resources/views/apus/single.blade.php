@extends('layouts.master')
@section('content')
    @section('title', 'APU ' . $apu->serial )
    <div class="container-fluid jumbotron td-jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <dl class="list-unstyled">
                        <dt><h5>Model</h5></dt>
                        <dd><h2>{{ $apu->model }}<p></p><small>{{ $apu->serial }}</small></h2></dd>
                    </dl>

                </div>
                <div class="col-sm-4">
                <h5>Listed by</h5>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="..." alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">TAG Aero</h4>
                            TAG Aero is the leading manufactuerer of etc..
                            <p></p>
                            <i class="fa fa-phone" aria-hidden="true"></i> +1 407-614-8314 | <i class="fa fa-envelope" aria-hidden="true"></i> info@tag.aero
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<div class="container">
    @if (Auth::check())
    @if( Auth::user()->id == $apu->owned_by )
    <div class="row well">
        <div class="col-sm-6">
           <h4>This is your listing. It will expire in {{ $apu->days_to_go }} days.</h4>
        </div>
        <div class="col-sm-6">
            <a href="{{ route('apus.edit', $apu->id) }}" class="btn btn-default pull-right">Edit</a>
        </div>
    </div>
    @endif
    @endif



    <div class="row">
        <div class="col-sm-8">
<div class="row">
            <div class="col-sm-6">
                <h4>Details</h4>
                <hr>
                <dl class="dl-horizontal">
                    <dt>Model</dt>
                    <dd>{{ $apu->model }}</dd>
                    <dt>Part Number</dt>
                    <dd>{{ $apu->part_number }}</dd>
                    <dt>Serial Number</dt>
                    <dd>{{ $apu->serial }}</dd>
                    <dt>Condition</dt>
                    <dd>{{ $apu->condition }}</dd>
                    <dt>Sale Type</dt>
                    <dd>{{ $apu->sale_type }}</dd>
                    <dt>Location</dt>
                    <dd>{{ $apu->location }}</dd>
                </dl>
            </div>

            <div class="col-sm-6">
                <h4>Tag Details</h4>
                <hr>
                <dl class="dl-horizontal">
                    <dt>Tagged by</dt>
                    <dd>{{ $apu->tagged_by }}</dd>
                    <dt>Tag Date</dt>
                    <dd>{{ $apu->tag_date }}</dd>
                    <dt>Tag Details</dt>
                    <dd>{{ $apu->details }}</dd>
                    <dt>Trace</dt>
                    <dd>{{ $apu->trace }}</dd>
                </dl>
            </div>
</div>
            <div class="row">
            <div class="col-sm-6">
                <h4>Cycles</h4>
                <hr>
                <dl class="dl-horizontal">
                    <dt>TSN</dt>
                    <dd>{{ $apu->tsn }}</dd>
                    <dt>CSN</dt>
                    <dd>{{ $apu->csn }}</dd>
                    <dt>TSLSV</dt>
                    <dd>{{ $apu->tslsv }}</dd>
                    <dt>CSLSV</dt>
                    <dd>{{ $apu->cslsv }}</dd>
                </dl>
            </div>
</div>


        </div>
        <div class="col-sm-4">
            <h5>Related APUs</h5>
            <hr>
        </div>

    </div>
</div>



@endsection