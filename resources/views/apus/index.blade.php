@extends('layouts.master')
@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
    @endsection
@section('title','Auxiliary Power Units')
@section('content')
<div class="container">
    <div class="row">
       <div class="col-xs-12">
           <h1>Auxiliary Power Units</h1>

           <hr>

           <table id="listings" class="display" cellspacing="0" width="100%">
               <thead>
               <tr>
                   <th>Model</th>
                   <th>PN</th>
                   <th>SN</th>
                   <th>Cond.</th>
                   <th>Trace</th>
                   <th>TSN</th>
                   <th>CSN</th>
                   <th>TSLSV</th>
                   <th>CSLSV</th>
                   <th>Controls</th>
               </tr>
               </thead>

               <tbody>

                @foreach($apus as $apu)
                   <tr>
                   <td>{{ $apu->model }}</td>
                       <td>{{ $apu->part_number }}</td>
                       <td>{{ $apu->serial }}</td>
                       <td>{{ $apu->condition }}</td>
                       <td>{{ $apu->trace }}</td>
                       <td>{{ $apu->tsn }}</td>
                       <td>{{ $apu->csn }}</td>
                       <td>{{ $apu->tslsv }}</td>
                       <td>{{ $apu->cslsv }}</td>
                       <td><a class ="btn btn-default btn-sm" href="{{ route('apus.show',$apu->id) }}">View</a>
                            <a class="btn btn-default btn-sm"><i class="fa fa-star" aria-hidden="true"></i></a>
                           <a class="btn btn-default btn-sm"><i class="fa fa-envelope-o" aria-hidden="true"></i></i></a>
                       </td>

                   </tr>
                    @endforeach


               </tbody>
           </table>

       </div>

    </div>
</div>
@endsection

