@include('includes._header')

<div id="login-overlay" class="modal-dialog">

    <a href="{{ route('home') }}"><img  style="width: 250px; display:block; margin: 0 auto !important; margin-bottom:45px !important;" src="/src/images/td-logo-white-bg.svg" alt="Teardown.AERO" onerror="this.src='/src/images/td-logo-white-bg.png'; this.onerror=null;" /></a>

    <div class="modal-content">
        <div class="modal-header">

        </div>
        <div class="modal-body">

            @yield('admincontent')

        </div>
    </div>