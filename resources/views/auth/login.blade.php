@extends('auth.authmaster')

@section('admincontent')
    <div class="row">
        <div class="col-sm-6">

            <form class="form" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="username" class="control-label">Email</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                    <span class="help-block"></span>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label">Password</label>
                    <input id="password" type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                    <span class="help-block"></span>
                </div>
                <div id="loginErrorMsg" class="alert alert-error hide">Wrong username or password</div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" id="remember"> Remember login
                    </label>
                    <p class="help-block">  <a href="{{ url('/password/reset') }}">Forgot Your Password?</a></p>
                </div>
                <button type="submit" value="login" name="submit" class="btn btn-default btn-block">Login</button>
            </form>

        </div>
        <div class="col-sm-6">
            <p class="lead">Register now for <span class="text-primary">FREE</span></p>
            <ul class="list-unstyled" style="line-height: 2">
                <li><span class="fa fa-check text-success"></span> Get up-to-date notifications!</li>
                <li><span class="fa fa-check text-success"></span> Save & share listings!</li>
                <li><span class="fa fa-check text-success"></span> Browse engines, APUs, and more!</li>
                <li><span class="fa fa-check text-success"></span> Free marketplace ads!</li>
                <li><span class="fa fa-check text-success"></span> Free membership!</li>

            </ul>
            <p><a href="{{ url('/register') }}" class="btn btn-primary btn-block">Register Now</a></p>
        </div>
    </div>


@endsection