@extends('auth.authmaster')

@section('admincontent')


    <div class="row">

        <form class="form" role="form" enctype="multipart/form-data" method="POST" action="{{ url('/register') }}">

        {{ csrf_field() }}
<div class="col-sm-4">
    <input type="file" class="form-control" name="avatar" value="{{ old('avatar') }}">

    @if ($errors->has('avatar'))
        <span class="help-block">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                    </span>
    @endif

</div>


        <div class="col-sm-8">



            {{--NAME--}}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Name *</label>

                {!! Form::text('name', old('name'), ['class'=>'form-control']) !!}


                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif

            </div>



            {{--EMAIL--}}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">E-Mail Address *</label>

                {!! Form::email('email', old('email'), ['class'=>'form-control']) !!}

                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif

            </div>




            {{--PHONE NUMBER--}}
            <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                <label for="email">Phone Number</label>

                {!! Form::text('phone_number', old('phone_number'), ['class'=>'form-control']) !!}

                @if ($errors->has('phone_number'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                @endif
            </div>




            {{--COMPANY--}}
            <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                <label for="email">Company</label>

                {!! Form::text('company', old('company'), ['class'=>'form-control']) !!}

                @if ($errors->has('company'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
                @endif
            </div>





            {{--POSITION--}}

            <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                <label for="email">Position</label>

                {!! Form::text('position', null, ['class'=>'form-control']) !!}

                @if ($errors->has('position'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                @endif
            </div>




            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">Password</label>


                        {!! Form::password('password', ['class'=>'form-control']) !!}

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation" class="control-label">Confirm</label>

                        {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}


                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif

                    </div>
                </div>
            </div>









            <hr>
            <strong>Email Preferences</strong>
            <p></p>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        {!! Form::checkbox('daily_email', '1') !!} Receive <strong>daily</strong> listings updates
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        {!! Form::checkbox('weekly_email', '1') !!} Receive <strong>weekly</strong> listings updates
                    </label>
                </div>
            </div>



            <div class="form-group">
                <span class="help-block">By registering on Teardown.AERO, you are agreeing to our Terms of Service.</span>
                {!! Form::submit('Complete Registration', ['class'=>'btn btn-primary btn-block']) !!}


            </div>
            {!! Form::close() !!}
        </div>


    </div>


@endsection
