@extends('dashboard.dashboardmaster')


@section('admincontent')






<div class="panel panel-default">
    <div class="panel-heading">Edit Profile</div>

    <div class="panel-body">


        {!! Form::open([

       'method'=>'POST',
       'class'=>'form',
       'url' => route('dashboard.updateuser'),
        'files' => true

       ]) !!}

        {{ Form::model($user, ['route' => ['dashboard.profile']]) }}


            <div class="col-sm-4">

                @if($user->avatar == null)


                <img style="margin-bottom:15px;" src="{{ Storage::url('td_user_placeholder.jpg') }}" class = "img-rounded"/>

                @else

                    <img style="margin-bottom:15px;" src="{{ $user->avatar() }}" class = "img-rounded"/>

                @endif

            <div class="form-group">
                {{ Form::file('avatar') }}

                </div>
                <span class="help-block"><small>For best results, images should have a width of 200px and height of 140px. Only .jpg images are allowed.</small></span>


                    @if($user->avatar)
                    <p><a style="margin-top:20px;" class="btn btn-default btn-xs" href="{{ route('dashboard.removeavatar') }}">Remove Picture</a></p>

                @endif
            </div>
            <div class="col-sm-8">
                <div class="form-group @if ($errors->has('name')) has-error @endif">
                    <label for="name">Name</label>
                    {{ Form::text('name', null, ['class'=>'form-control']) }}
                </div>

                <div class="form-group @if ($errors->has('email')) has-error @endif">
                    <label for="email">Email</label>
                    {{ Form::text('email', null, ['class'=>'form-control']) }}
                </div>

                <div class="form-group @if ($errors->has('phone_number')) has-error @endif">
                    <label for="phone_number">Phone</label>
                    {{ Form::text('phone_number', null, ['class'=>'form-control']) }}
                </div>

                <div class="form-group @if ($errors->has('company')) has-error @endif">
                    <label for="phone_number">Company Name</label>
                    {{ Form::text('company', null, ['class'=>'form-control']) }}
                </div>

                <div class="form-group @if ($errors->has('position')) has-error @endif">
                    <label for="phone_number">Position</label>
                    {{ Form::text('position', null, ['class'=>'form-control']) }}
                </div>

                <hr>
                <strong>Email Preferences</strong>
      <p></p>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        {{ Form::checkbox('daily_email', '1') }} Receive <strong>daily</strong> listings updates
                    </label>
                </div>
            </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            {{ Form::checkbox('weekly_email', '1') }} Receive <strong>weekly</strong> listings updates
                        </label>
                    </div>
            </div>






                {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}

            </div>



        {!! Form::close() !!}





</div>


    @endsection