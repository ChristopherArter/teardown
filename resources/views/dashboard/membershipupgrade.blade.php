@extends('dashboard.dashboardmaster')



@section('admincontent')

    <div class="panel panel-default">
        <div class="panel-heading">Upgrade Confirmation</div>
        <div class="panel-body">

            <div class="col-sm-4">
<?php
                // Access card information
                $defaultID = $customer->default_source;
                $default = $customer->sources->retrieve($defaultID);

                $sources = $customer->sources->all(array(
                'object' => 'card'));

                ?>



                    {!! Form::open([

                            'url' => 'dashboard/membership/upgrade/process',
                            'class'=>'payment-form',
                            'method'=>'post']


                    ) !!}
                    <div class="form-group">
                        <label>Payment Method:</label>
                        <select name="card_id" class="form-control">
                            <option value="{{ $default->id }}" selected>(default) {{ $default->brand }} - ({{ $default->last4 }})</option>

                            @foreach($customer->sources->data as $source)
                                @if($source->id !== $default->id)

                                    <option value="{{ $source->id }}">{{ $source->brand }} - ({{ $source->last4 }})</option>

                                @endif
                            @endforeach

                        </select>
                        <p></p>
                        <p class="text-right"><small><a href="#">Manage Payment Options</a></small></p>

                    </div>

                    <hr>
                    <h3 class="text-center">${{ $total/100 }}</h3>
                    <p class="text-center">Per Month</p>

                    {{ Form::submit('Complete Purchase', ['class'=>'btn btn-primary btn-lg btn-block']) }}



                    {!! Form::close() !!}








            </div>

            <div class="col-sm-8">

                <h3>Order Summary</h3>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Desc</th>

                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Teardown.Aero Premium Membership</td>
                        <td>${{ $total / 100 }} / mo.</td>

                    </tr>

                    </tbody>
                </table>
            </div>






        </div>
    </div>


    @endsection