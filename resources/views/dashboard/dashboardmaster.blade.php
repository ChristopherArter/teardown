@extends('layouts.master')
@section('title')
    Dashboard
    @endsection

@section('content')
    <div class="container">
        <div class="row row-offcanvas row-offcanvas-left">
            {{--SIDE NAV BAR--}}
<div class="sidebar-loader">
    @include('dashboard.partials._adminsidebar')
</div>



            <div class="col-xs-12 col-sm-9 content">
                @if(isset($message))

                    <div class="panel-success panel"> {{ $message }}</div>


                @endif


               @yield('admincontent')
            </div><!-- content -->
        </div>
    </div>
    @endsection



