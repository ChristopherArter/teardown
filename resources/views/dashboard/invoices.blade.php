@extends('dashboard.dashboardmaster')


@section('admincontent')
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Order History</strong>
        </div>

        <div class="panel-body">

            @if(isset($invoices))
    @include('dashboard.partials.sections._invoices')

                @else
            <p class="text-center">No invoices have been created at this time.</p>
            @endif
        </div>
    </div>

@endsection