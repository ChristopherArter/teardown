@extends('dashboard.dashboardmaster')


@section('admincontent')

    <div class="row">
        <div class="col-xs-12">
            <h5>My Listings</h5>
        </div>

    </div>


<div class="panel">
    <ul id="myTab1" class="nav nav-tabs nav-justified">
        <li class="active"><a href="#engines" data-toggle="tab">Engines</a></li>
        <li class=""><a href="#apus" data-toggle="tab">APUs</a></li>
        <li class=""><a href="#aircraft" data-toggle="tab">Aircraft</a></li>
        <li class=""><a href="#market" data-toggle="tab">Market</a></li>


    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="engines">

            @include('dashboard.partials.sections._listings',['units'=>$engines])

        </div>
        <div class="tab-pane fade" id="apus">
            @include('dashboard.partials.sections._listings',['units'=>$apus])
        </div>
        <div class="tab-pane fade" id="aircraft">
     Planes n shit
        </div>
        <div class="tab-pane fade" id="market">
           @foreach($marketPosts as $marketPost)


                  <p> <strong>{{ $marketPost->market_title }}</strong></p>
                   <p>{{ $marketPost->market_body }}</p>
                   <p> <small><em>Posted on {{ Carbon::createFromTimestamp(strtotime($marketPost->created_at))->toFormattedDateString() }}</em>
                           {{ Form::open(array('route' => array('market.destroy', $marketPost->id), 'method' => 'delete')) }}
                           <button type="submit" class="btn btn-default btn-xs">Delete</button>
                           {{ Form::close() }}
                       </small> </p>

<hr>

               @endforeach
        </div>


    </div>
</div>




    @endsection