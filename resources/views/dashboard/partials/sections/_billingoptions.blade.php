
        @foreach($cards['data'] as $card)


            <div class="panel

                @if($customer['default_source'] == $card['id'])
                    panel-primary
                    @else
                    panel-default
                    @endif" data-stripe-card-id = {{$card['id'] }}>
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">

                        Billing Information for
                        <?php
                        // Display the card icon. Too bad there are no switch statements in Blade :(
                        switch ($card['brand']) {

                            case "Visa":
                                echo '<i class="fa fa-cc-visa" aria-hidden="true"></i>';
                                break;

                            case "MasterCard":
                                echo '<i class="fa fa-cc-mastercard" aria-hidden="true"></i>';
                                break;

                            case "American Express":
                                echo '<i class="fa fa-cc-amex" aria-hidden="true"></i>';
                                break;

                            case "Discover":
                                echo '<i class="fa fa-cc-discover" aria-hidden="true"></i>';
                                break;

                            case "JCB":
                                echo '<i class="fa fa-cc-jcb" aria-hidden="true"></i>';
                                break;

                            case "Diners Club":
                                echo '<i class="fa fa-cc-diners-club" aria-hidden="true"></i>';
                                break;

                            case "Unknown":
                                echo '<i class="fa fa-credit-card-alt" aria-hidden="true"></i>';
                                break;


                        }
                        ?>  ({{ $card['last4'] }})


                        <?php

                        if ($customer['default_source'] == $card['id']){
                        ?>
                        <span class="label label-default">Default</span>
                        <?php
                        }

                        ?>
                    </h4>
                </div>

                <div class="panel-body">

                    <dl class="dl-horizontal">
                        <dt>Name on Card</dt>
                        <dd>{{ $card['name'] }}</dd>
                        <p></p>
                        <dt>Card Number</dt>
                        <dd>**** **** **** {{ $card['last4'] }}</dd>
                        <p></p>
                        <dt>Expiration</dt>
                        <dd>{{ $card['exp_month'] }}/{{ $card['exp_year'] }}</dd>

                    </dl>
                    <p></p>
                    <p></p>

                    <div class="row">
                        @if($customer['default_source'] !== ['id'])
                            <div class="col-xs-2">
                                {!! Form::open([ 'url' => route('dashboard.newDefault'), 'method'=>'post', ] ) !!}
                                {{ Form::hidden('newDefault', $card['id']) }}

                                {{ Form::submit('Set as Default', ['class'=>'btn btn-default btn-sm']) }}
                                {!! Form::close() !!}
                            </div>
                        @endif

                        <div class="col-xs-2">

                            {!! Form::open([ 'url' => route('dashboard.deleteCard'), 'method'=>'post', ] ) !!}
                            {{ Form::hidden('deleteID', $card['id']) }}

                            {{ Form::submit('Remove Card', ['class'=>'btn btn-default btn-sm']) }}
                            {!! Form::close() !!}
                        </div>
                    </div>




                </div>

            </div>

        @endforeach

