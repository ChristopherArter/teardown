@section('stylesheets')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#invoices_table').dataTable({
                "order": [[ 1, "desc" ]]
            } );
        });
    </script>


    @endsection


<table class="table" id="invoices_table">
    <thead>
    <tr>
        <th>Invoice Number</th>
        <th>Created</th>
        <th>Status</th>

        <th>Total</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>

    @foreach($invoices as $invoice)


    <?php 

  



    $time = Carbon::parse($invoice->created_at)->toDateString(); 

    ?>


        <tr>
            <td>{{ $invoice->invoice_number }}</td>
            <td>
                {{ $time }}
            </td>
            <td>{{ $invoice->status }}</td>

            <td>
                ${{ $invoice->total / 100 }}

            </td>
            <td>


<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#invoice_items_modal_{{$invoice->id}}">View</button>

<!-- Modal -->
<div id="invoice_items_modal_{{$invoice->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Invoice #{{$invoice->invoice_number}}</h4>
      </div>
      <div class="modal-body">


<div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <address>
                    <strong>From:</strong><br>
                        Teardown Aero LLC<br>
                        1226 Winter Garden Vineland Rd.<br>
                        Bld 5 Ste 108<br>
                        Winter Garden, FL 34787
                    </address>
                </div>
                <div class="col-xs-6">
                    <address>
                    <strong>Billed To:</strong><br>
                        {{ Auth::user()->name }}<br>
                        {{ Auth::user()->email}}<br>
                       
                    </address>
                                        <address>
                        <strong>Order Date:</strong><br>
                        {{ $time }}<br><br>
                    </address>
                </div>
            </div>

        </div>
    </div>
    




      <table class="table table-condensed">

      <thead>
        <th><label>Description</label></th>
        <th><label>Amount</label></th>
      </thead>


      <tbody>


      @if(!$invoice->lineItems->isEmpty())

      @foreach($invoice->lineItems as $item)
      
        <tr>
        <td>{{ $item['description'] }}</td>
        <td class="text-right">${{ $item['amount'] / 100 }}</td>
        </tr>

      @endforeach

                            <tr>
                                   
                                    
                                    <td class="thick-line text-right"><strong>Total</strong></td>
                                    <td class="thick-line text-right">${{$invoice->total /100 }}</td>
                                </tr>


      @endif






      </tbody>

      </table>





      </div>
      <div class="modal-footer">
     <a class="btn btn-primary" href="{{ route('dashboard.billing.download', $invoice->id) }}"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download PDF</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


             <a class="btn btn-default btn-sm" href="{{ route('dashboard.billing.download', $invoice->id) }}"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download PDF</a>

            </td>

        </tr>
    @endforeach
    </tbody>




</table>