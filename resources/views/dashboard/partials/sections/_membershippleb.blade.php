{{--IF PLEB--}}
<dl class="dl-horizontal">
    <dt>Membership Level</dt>
    <dd><span class="label label-default">Basic</span></dd>
</dl>

<div class="col-sm-4">
    <div class="pricing">
        <ul>
            <li class="unit price-primary">
                <div class="price-title">
                    {{--<h3>{{ $sub['plan']['amount'] / 100 }}</h3>--}}
                    <p>per month</p>
                </div>
                <div class="price-body">
                    <h4>Basic</h4>
                    <p>Lots of clients &amp; users</p>
                    <ul>
                        <li><strong><em>Unlimited</em></strong> Engine Listings</li>
                        <li><strong><em>Unlimited</em></strong> APU Listings</li>
                        <li><strong><em>ActiveSearch™</em></strong> Automatic Sourcing</li>
                        <li>Personal Listings Inventory Page</li>
                    </ul>
                </div>
                <div class="price-foot">
                    <a href="{{ url('dashboard/membership/upgrade') }}" class="btn btn-primary">Instant Upgrade</a>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="col-sm-6">
    {{--UNLMITED LISTINGS--}}
    <p><strong>Unlimited Listings</strong></p>
    <p>That's right. We are offering <em>unlimited</em> listings for both engines and APUs. If you list more two or three units in a month, the Premium Membership is an easy choice to save you money. </p>

    {{--ACTIVE SEARCH--}}
    <p><strong>ActiveSearch™</strong></p>
    <p>Instead of spending hours searching for just the right unit for your requirement, let our software do the work for you with ActiveSearch. ActiveSearch alerts you soon after a unit matching your criteria is listed on Teardown.AERO.</p>


</div>
