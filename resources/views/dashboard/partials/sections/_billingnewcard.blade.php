
@section('scripts')

    {{ Html::script('/src/js/vendor/parsley.js') }}
    <script type="text/javascript">
        $('.td-dash-cc-form').parsley();
    </script>

@endsection

<div class="modal-body">
    <span class="payment-errors"></span>
{{--FORM START--}}
{!! Form::open([

        'url' => 'dashboard/addcreditcard',
        'class'=>'form-horizontal',
        'method'=>'POST',
        'id' => 'td-dash-cc-form'
        ]


) !!}
    <input type="hidden" id="STRIPE_KEY" value = "{{ env('STRIPE_KEY') }}"/>
<div class="row">
    <span class="payment-errors"></span>
</div>

    <fieldset>


    {{--NAME ON CARD--}}
    <div class="form-group" id="">
        <label class="col-sm-3 control-label" for="card-holder-name">Name on Card</label>
        <div class="col-sm-9">
            {!! Form::text(null, null, [

                'class'                         => 'form-control',
                'required'                      => 'required',
                'data-stripe'                   => 'name'

            ]) !!}
        </div>
    </div>


    {{--CARD NUMBER--}}
    <div class="form-group">
        <label class="col-sm-3 control-label" for="card-number">Card Number</label>
        <div class="col-sm-9">
            {!! Form::text(null, null, [
            'class'             => 'form-control',
            'data-stripe'       =>'number',
            'required' ]) !!}
        </div>
    </div>


    {{--EXPIRATION DATE--}}
    <div class="form-group">
        <label class="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
        <div class="col-sm-9">
            <div class="row">

                {{--EXP MONTH--}}
                <div class="col-xs-3">
                    {!! Form::selectMonth(null, null, ['class' => 'form-control', 'data-stripe' => 'exp_month', 'required'], '%m') !!}
                </div>

                {{--EXP YEAR--}}
                <div class="col-xs-3">
                    {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, ['class' => 'form-control', 'data-stripe' => 'exp_year', 'required' ]) !!}
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-3 control-label" for="cvc">CVC</label>
        <div class="col-sm-3">
            {!! Form::text(null, null, [

                        'class'                         => 'form-control',
                        'data-stripe'                   =>'cvc',
                        'data-parsley-type'             =>'number',
                        'data-parsley-required-message' => 'Only numbers allowed',
                        'required',


                        ]) !!}
        </div>
    </div>


    <div class="modal-footer">
        <img src="{{ Storage::url('powered_by_stripe.png') }}" class = "pull-left"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class'=>'btn btn-primary', 'id'=>'submitBtn']) !!}
    </div>
</fieldset>
</div>

{!! Form::close() !!}
