{{--@foreach($units as $unit)--}}

    {{--{{ var_dump( $unit->post_type ) }}--}}
    {{--@endforeach--}}



<div class="table-responsive ">
    <table class="table">
        <thead>
        <tr>

            <th>Model</th>
            <th>Part Number</th>
            <th>Serial Number</th>
            <th>Condition</th>
            <th>Days Remaining</th>
            <th>Auto-Renew</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        @foreach($units as $unit)
        <tr
        @if($unit->days_to_go == 0)
            class="danger"
            @endif


        >

            <td>{{ $unit->model }} @if($unit->post_type == "engine") {{ $unit->minor_variant }}@endif


            </td>
            <td>{{ $unit->part_number }}</td>
            <td>{{ $unit->serial }}</td>
            <td>{{ $unit->condition }}</td>
            <td>{{ $unit->days_to_go }}</td>
            <td>@if( $unit->auto_renew ) <i class="fa fa-check" aria-hidden="true"></i> @endif</td>
            <td>


                <a href="{{ route(''.$unit->post_type.'s.edit', $unit->id) }}" class="btn btn-default btn-xs">Edit</a></td>
        </tr>
        @endforeach


        </tbody>
    </table>
</div>