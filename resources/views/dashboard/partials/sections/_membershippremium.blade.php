<?php

// Set timestamps to Carbon objects
$startDate = Carbon::createFromTimestamp($sub['created'])->toFormattedDateString();
$renewalDate = Carbon::createFromTimestamp($sub['current_period_end'])->toFormattedDateString();

?>

<dl class="dl-horizontal">
    <dt>Membership Level</dt>
    <dd><span class="label label-success">Premium</span></dd>
    <dt>Start Date</dt>
    <dd>{{ $startDate }}</dd>
    <dt>Renews</dt>
    <dd>{{ $renewalDate }}</dd>
</dl>



{!! Form::open([

         'url' => 'dashboard/membership/cancel/process',
         'class'=>'payment-form',
         'method'=>'post']


 ) !!}
{{ Form::submit('Cancel Membership', ['class'=>'btn btn-danger btn-xs']) }}

{{ Form::close() }}