<div class="col-sm-6 col-sm-3 sidebar-offcanvas" role="navigation">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                {{--<div class="col-xs-4">--}}
                    {{--<img style="margin-bottom:15px;" src="{{ Storage::url( 'public/avatars/'.$user->id.'/'.$user->id.'.jpg') }}" class = "img-thumbnail"/>--}}
                {{--</div>--}}
                <div class="col-xs-12">
                    <ul class="list-unstyled">
                        <li><strong>  {{ $user->name }}</strong></li>
                        <li><small>{{ $user->position }}</small></li>
                 <?php if($user->company){
                            ?>
                            <li><small>{{ $user->company }}</small></li>
                       <?php } ?>
                        <li><small>{{ $user->email }} @if($user->phone_number)| {{ $user->phone_number }}@endif</small></li>

                    </ul>
<small><a href="{{ route('dashboard.profile') }}">(Edit)</a></small>
                </div>

            </div>


        </div>

    </div>
    <div class="list-group panel">
        {{--<li class="list-group-item"><i class="glyphicon glyphicon-align-justify"></i> <b>Dashboard</b></li>--}}
        <a class="list-group-item" href="{{ route('dashboard.profile') }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Profile</a>
        <a class="list-group-item" href="{{ route('dashboard.messages') }}"><i class="fa fa-bell" aria-hidden="true"></i> Notifications

        <?php if ($user->unreadMessageCount()){
                ?>

            <span class="label label-success">{{ $user->unreadMessageCount() }}</span>
            <?php
            }

            ?>


        </a>
        <a class="list-group-item"  href="{{ route('dashboard.listings') }}"><i class="fa fa-thumb-tack" aria-hidden="true"></i> Listings</a>


        <a class="list-group-item"  href="{{ route('dashboard.membership') }}"><i class="fa fa-address-book" aria-hidden="true"></i> Membership</a>
        <a class="list-group-item"  href="{{ route('dashboard.invoices') }}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
        <a  class="list-group-item" href="{{ route('dashboard.billing') }}"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Payment Options</a>
        <a  class="list-group-item" href="{{ route('logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out</a>

    </div>
</div>
