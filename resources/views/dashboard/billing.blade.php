@extends('dashboard.dashboardmaster')

@section('title')
    Dashboard - Billing Options
@endsection




@section('admincontent')


    @if(session('inbound_url_message'))

        <div class="alert-success alert"> {{ session('inbound_url_message') }}</div>

        @endif



    @if($hasStripe)


    <div class="row" style="margin-bottom:20px;">

        <div class="col-sm-8">
            @if(isset($message))

                <div class="panel-success panel"> {{ $message }}</div>


                @endif
        </div>


        <div class="col-sm-4">

            <a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#new_card">Add New Card</a>
            <!-- Modal -->
            <div id="new_card" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add New Card</h4>
                        </div>

                           @include('dashboard.partials.sections._billingnewcard')

                    </div>

                </div>
            </div>
        </div>
        <br>
    </div>

@endif

    <div class="panel panel-default">


        <div class="panel-heading">
            <strong>Billing Options</strong>
        </div>

        <div class="panel-body">


            @if(!$hasStripe)

                @include('dashboard.partials.sections._billingnewcard')

                @else

                @include('dashboard.partials.sections._billingoptions')


                @endif

        </div>
    </div>

@endsection

@section('stylesheets')
    <input type="hidden" id="STRIPE_KEY" value="{{ env('STRIPE_KEY') }}"/>
    <script type="text/javascript">


        var StripeKey = $('#STRIPE_KEY').val();
        console.log(StripeKey);

    </script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    {{ Html::script('src/js/dashboard.js') }}
@endsection


