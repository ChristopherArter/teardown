@extends('dashboard.dashboardmaster')


@section('admincontent')



    <div class="panel panel-default">
        <div class="panel-heading"><strong>Membership</strong></div>
        <div class="panel-body">


            @if(isset($sub))

                @include('dashboard.partials.sections._membershippremium')

                @else

                @include('dashboard.partials.sections._membershippleb')

            @endif



        </div>
    </div>

    @endsection