@extends('dashboard.dashboardmaster')

@section('stylesheets')
    {{ Html::script('src/js/messages.js') }}
    @endsection
@section('admincontent')


    <div class="messages-content-loader">
       

    	<div class="list-group">

    @foreach($msgs as $msg)
        <a href="{{ route('message.show', $msg->id) }}"  class=" list-group-item">
            <!-- Modal -->



            @if($msg->unread == true)

                <span class="label label-success">New</span> <strong>{{ $msg->subject }} <small>(Unread)</small></strong>

            @else

                {{ $msg->subject }}

            @endif



            <span class="pull-right">{{ $msg->created_at }}</span>



        </a>
        <div data-read = "{{ $msg->unread }}" data-message-id = "{{ $msg->id }}" class="modal td-message-modal fade" id="message_{{ $msg->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ $msg->subject }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>{{ $msg->body }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    @endforeach



    
</div>
{{ $msgs->render() }}




    </div>
  
    @endsection