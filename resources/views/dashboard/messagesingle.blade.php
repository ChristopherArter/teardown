@extends('dashboard.dashboardmaster')

@section('stylesheets')
    {{ Html::script('src/js/messages.js') }}
    @endsection
@section('admincontent')


<div class="panel panel-default">


        <div class="panel-heading">
            <strong>{{ $msg->subject }}</strong>
        </div>

        <div class="panel-body">


        {{ $msg->body }}
<?php 

$time = Carbon::parse($msg->created_at)->toDayDateTimeString(); 

?>
                
        </div>
        <div class="panel-footer">
	<small>Sent on <strong>{{ $time }}</strong> | <a href="{{ route('dashboard.messages') }}">Return to Messages</a></small>
        </div>
    </div>
  
@endsection