@extends('layouts.master')

@section('content')

    <div id="td-home-header-vid" class="container-fluid">
        <div class="container">
            {{--MAIN HEADER VID--}}
            <div class="col-xs-12 text-center">
                <h1 style="padding-top:20px; color:#fff;">Aviation parts, from the source.</h1>
                <h3 style="color:#fff;">Your best resource for As-Removed materials, directly from aircraft teardowns.</h3>
                {!! Form::open(['method' => 'POST', 'route' => 'email.signup', 'class' => 'form-horizontal']) !!}
                
                 <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                     {!! Form::label('email', 'Input', ['class' => 'col-sm-3 control-label']) !!}
                     <div class="col-sm-9">
                         {!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
                         <small class="text-danger">{{ $errors->first('email') }}</small>
                     </div>
                 </div>
                
                    <div class="btn-group pull-right">
                       
                        {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
                    </div>
                
                {!! Form::close() !!}
                <p><a href="#" class="btn btn-primary">Let's see it.</a></p>
            </div>

        </div>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-sm-4">



<h4><strong>Aircraft</strong>
                   <p><small>Aircraft in dissasembly</small></p></h4>
<?php

// Quick and dirty avatar for the template
// 
$avatarUrl = $user->avatar();


?>
                @foreach($aircrafts as $aircraft)

                     @includeIf('includes._aircraftlistitem', ['aircraft' => $aircraft, 'avatar' => $avatarUrl])

                @endforeach


                <div class="text-center">
<a href="{{ route('aircraft.index') }}" class="text-center btn btn-default btn-sm">See All</a>
</div>

            </div>

            <div class="col-sm-4">
     
                   <h4><strong>Engines</strong>
                   <p><small>For sale, lease or exchange.</small></p></h4>

                                 @foreach($engines as $engine)

                     @includeIf('includes._engineapulistitem', ['listing' => $engine])

                @endforeach

                        <div class="text-center">
                        <a href="{{ route('engines.index') }}" class="text-center btn btn-default btn-sm">See All</a>
                        </div>

            </div>



                        <div class="col-sm-4">

                        <h4><strong>Auxiliary Power Units</strong>
                   <p><small>For sale, lease or exchange.</small></p></h4>

                        @foreach($apus as $apu)

                            @includeIf('includes._engineapulistitem', ['listing' => $apu])

                        @endforeach

<div class="text-center">
<a href="{{ route('apus.index') }}" class="text-center btn btn-default btn-sm">See All</a>
</div>


                        </div>

            </div>

        </div>
    </div>

    @endsection

@section('scripts')
    {{ Html::script('src/js/vendor/jquery.vide.min.js') }}
    {{ Html::script('src/js/home.js') }}

@endsection