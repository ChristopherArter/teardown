$(document).ready(function(){
    // JS for new listing page.
    $('[data-toggle="tooltip"]').tooltip({'placement': 'bottom'});


    // Set 8130 fields on load based on condition field value. Used for redirection withInput() method.
    if ($('#condition').val() == 'SV' ||
        $('#condition').val() == 'OH' ||
        $('#condition').val() == 'NE' ) {

    $('.listing-form').find('.8130').prop('disabled', false);
    }

    // Enable 8130 fields if correct conditions
    $('#condition').on('change', function(){

        var value = $(this).val();
                if (value == "SV" ||
                    value == "OH" ||
                    value == "NE" ){

           $('.listing-form').find('.8130').prop('disabled', false);
        } else {
            $('.listing-form').find('.8130').prop('disabled', true);
        }
    });

    // var filename = $('#upload').val();
    // var lastIndex = filename.lastIndexOf("\\");
    // if (lastIndex >= 0) {
    //     filename = filename.substring(lastIndex + 1);
    // }
    // $('#filename').val(filename);
    //
    //
    // $('#upload').change(function() {
    //     var filename = $(this).val();
    //     var lastIndex = filename.lastIndexOf("\\");
    //     if (lastIndex >= 0) {
    //         filename = filename.substring(lastIndex + 1);
    //     }
    //     $('#filename').html('<i class="fa fa-file-pdf-o" aria-hidden="true"></i>' + filename);
    // });


});