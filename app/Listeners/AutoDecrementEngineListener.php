<?php

namespace App\Listeners;

use App\Events\DailyTasks;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Engine;

class AutoDecrementEngineListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DailyTasks  $event
     * @return void
     */
    public function handle()
    {
        // Auto decrement Engines
        Engine::where('days_to_go', '>', 0)->where('status', '=', 'publish')->decrement('days_to_go', 1);
    }
}
