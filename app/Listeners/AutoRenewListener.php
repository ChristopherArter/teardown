<?php

namespace App\Listeners;

use App\Events\DailyTasks;
use App\Apu;
use App\Engine;
use Auth;
use App\User;
use App\Aircraft;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AutoRenewListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DailyTasks  $event
     * @return void
     */
    public function handle()
    {

//        // Get all APUs set for auto-renew
//        $apus = Apu::where('days_to_go', '=', 0)->where('status', '=', 'publish')->where('auto_renew', '=', true)->get();
//
//
//        if(!$apus->isEmpty())
//        {
//            foreach($apus as $apu)
//            {
//
//                $owner = User::find($apu->user()->get()[0]->id);
//
//            if($owner->isPremium())
//            {
//
//                $apu->renew();
//                $apu->save();
//
//            } else {
//
//                // purchase logic goes here.
//
//            }
//            }
//        }

        $users = User::all();

        foreach ($users as $user) {
            $user->autoRenew();
        }
    }
}
