<?php

namespace App\Listeners;

use App\Events\DailyEmailEvent;
use App\Mail\DailyListings;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\Notifications\ExpirationWarning;
use Auth;
use App\User;
use Apu;
use Engine;

class DailyEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */


    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExpiredListingEvent  $event
     * @return void
     */
    public function handle(DailyEmailEvent $event)
    {
        
//		Mail::to('christopherarter@gmail.com')->send(new DailyListings());
        


        foreach ($users->users as $user) {
            $apuListings = $user->apus()->where('days_to_go', '<', 5)->where('status', '=', 'publish')->get();
            $engineListings = $user->engines()->where('days_to_go', '<', 5)->where('status', '=', 'publish')->get();
            $user->notify(new ExpirationWarning($apuListings, $engineListings, $user));
        }
    }
}
