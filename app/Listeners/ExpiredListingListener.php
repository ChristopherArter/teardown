<?php

namespace App\Listeners;

use App\Events\DailyTasks;
use App\Events\ExpiredListingEvent;
use App\Mail\DailyListings;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Engine;
use App\Apu;

class ExpiredListingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExpiredListingEvent  $event
     * @return void
     */
    public function handle()
    {


        /*
         * Apus
         *
         */



        // Set listings to "expired" if they, well, expire.
        $expiredApus = Apu::where('days_to_go', '=', 0)->where('status', '=', 'publish')->where('auto_renew', '=', null)->get();

        // set all the 0 listings to expired
        foreach ($expiredApus as $item) {
            $item->status = 'expired';
            $item->save();
            event(new ExpiredListingEvent($item));
        }

        /*
         * Engines
         */


        // Add 30 days to each APU that is set to auto-renew
        Engine::where('days_to_go', '=', 0)->where('status', '=', 'publish')->where('auto_renew', '=', true)->update(['days_to_go'=> 30]);

        // Set listings to "expired" if they, well, expire.
        $expiredEngines = Engine::where('days_to_go', '=', 0)->where('status', '=', 'publish')->where('auto_renew', '=', null)->get();

        // set all the 0 listings to expired
        foreach ($expiredEngines as $item) {
            $item->status = 'expired';
            $item->save();
            event(new ExpiredListingEvent($item));
        }
    }
}
