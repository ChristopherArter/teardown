<?php

namespace App\Listeners;

use App\Events\NewApuListingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewApuListingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewApuListingEvent  $event
     * @return void
     */
    public function handle(NewApuListingEvent $apu)
    {
     //
    }
}
