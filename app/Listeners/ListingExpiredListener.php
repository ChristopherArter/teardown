<?php

namespace App\Listeners;

use App\Events\ListingExpiredEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ListingExpiredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ListingExpiredEvent  $event
     * @return void
     */
    public function handle(ListingExpiredEvent $event)
    {
        //
    }
}
