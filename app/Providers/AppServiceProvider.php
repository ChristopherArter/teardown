<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('includes.dashboard.partials._billingindex', function ($view) {
                $user = Auth::user();
                // Get user's stripe object
                \Stripe\Stripe::setApiKey("sk_test_QnrOrUxY0S43c6GEhiu9Nwhe");
                $customer = \Stripe\Customer::retrieve($user->stripe_id);
                $view->with('user', $user)->with('customer', $customer);
        });


        view()->composer('newlistings.includes._newlistingseditsidebar', function ($view) {

            $user = Auth::user();
            $apus = $user->apus()->where('status', '=', 'draft')->get();
            $engines = $user->engines()->where('status', '=', 'draft')->get();

            $view->with('apus', $apus)->with('engines', $engines);
        });

        view()->composer('newlistings.includes._paymentselectsidebar', function ($view) {

            // Get user's stripe object
            \Stripe\Stripe::setApiKey("sk_test_QnrOrUxY0S43c6GEhiu9Nwhe");
            $user = Auth::user();
            $customer = \Stripe\Customer::retrieve($user->stripe_id);

            $view->with('customer', $customer);
        });


        view()->composer('dashboard.partials._adminsidebar', function ($view) {
            $user = Auth::user();
            // Get user's stripe object


            $view->with('user', $user);
        });

        view()->composer('dashboard.partials.sections._messages', function ($view) {
            $user = Auth::user();

            $messages = $user->messages()->get();

            $view->with('messages', $messages);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
