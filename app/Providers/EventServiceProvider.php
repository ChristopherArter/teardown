<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\NewApuListingEvent::class => [ \App\Listeners\NewApuListingListener::class],
        \App\Events\ExpiredListingEvent::class => [\App\Listeners\ExpiredListingListener::class],
        \App\Events\DailyEmailEvent::class => [\App\Listeners\DailyEmailListener::class],
        \App\Events\DailyTasks::class => [

            \App\Listeners\AutoDecrementApuListener::class,
            \App\Listeners\AutoDecrementEngineListener::class,
            \App\Listeners\AutoRenewListener::class,
            \App\Listeners\ExpiredListingListener::class

        ],

        \App\Events\ListingExpiredEvent::class => ['ListingExpiredListener'],

        // Events that fire when listings are purchased
        \App\Events\ListingsPurchased::class => [

            'ListingsPurchasedEmail'
            
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
