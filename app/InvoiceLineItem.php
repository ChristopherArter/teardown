<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceLineItem extends Model
{
   
	protected $table = 'invoice_line_items';

	    public function invoice()
    {
        return $this->belongsTo(\App\Invoice::class);
    }



}
