<?php

namespace App;

use App\Scopes\ListingScope;
use Illuminate\Database\Eloquent\Model;

class Engine extends Model
{
    protected static function boot()
    {
        parent::boot();

//		static::addGlobalScope(new ListingScope);
    }
    public $price;
    public $post_type = 'engine';

    public function user()
    {

        return $this->belongsTo(\App\User::class, 'id', 'owned_by');
    }

   // Renew Engine
    public function renew()
    {

        $this->days_to_go = 30;
        $this->save();
    }


    public function orders()
    {
        return $this->belongsTo(\App\Order::class, 'id', 'apu_id');
    }

    function __construct()
    {

        $this->price = config('stripe.engine.price');
    }
}
