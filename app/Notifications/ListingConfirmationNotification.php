<?php

namespace App\Notifications;

use App\Mail\ListingConfirmationMail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ListingConfirmationNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $apus;
    public $engines;
    public $invoice;
    public $user;

    public function __construct($apus, $engines, $invoice, $user)
    {

        $this->apus = $apus;
        $this->engines = $engines;
        $this->invoice = $invoice;
        $this->user = $user;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new ListingConfirmationMail($this->apus, $this->engines, $this->invoice))
            ->to($this->user->email)
            ->from('info@teardown.aero');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
