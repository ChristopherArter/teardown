<?php

namespace App\Notifications;

use App\Mail\ExpiringSoonEmail;
use Illuminate\Bus\Queueable;
use App\User;
use App\Apu;
use App\Engine;
use App\Aircraft;
use Auth;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ExpirationWarning extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    protected $apus;
    protected $engines;
    protected $user;
    public function __construct($apus, $engines, $user)
    {
        $this->user = $user;
        $this->apus = $apus;
        $this->engines = $engines;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        return (new MailMessage)
//                    ->line('The introduction to the notification.')
//                    ->action('Notification Action', 'https://laravel.com')
//                    ->line('Thank you for using our application!');



        return (new ExpiringSoonEmail($this->apus, $this->engines))
            ->to($this->user->email)
            ->from('info@teardown.aero')
            ->view('mail.expiringsoon')
            ->with('apuListings', $this->apus)
            ->with('engineListings', $this->engines);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
