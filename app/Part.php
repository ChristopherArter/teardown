<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    public function manifest()
    {
        return $this->belongsTo(\App\Manifest::class);
    }
}
