<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use App\Apu;
use App\Engine;
use App\Message;
use App\ActiveSearch;
use Cartalyst\Stripe\Stripe;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\Storage;
use App\Notifications\ExpirationWarning;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use Billable;
    protected $fillable = [
        'name', 'email', 'password', 'avatar'
    ];

    protected $casts = [

        'settings' => 'json'

    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function apus()
    {

        return $this->hasMany(\App\Apu::class, 'owned_by', 'id');
    }
    
    function activeSearches()
    {
        
        return $this->hasMany(\App\ActiveSearch::class, 'user_id', 'id');
    }
    

    function marketPosts()
    {

        return $this->hasMany(\App\MarketPost::class, 'author_id', 'id');
    }

    function messages()
    {

        return $this->hasMany(\App\Message::class, 'user_id', 'id');
    }

    function unreadMessageCount()
    {

        $count = $this->messages()->where('unread', '=', '1')->count();

        if ($count == 0) {
            return null;
        } else {
            return $count;
        }
    }


    function invoices()
    {

        return $this->hasMany(\App\Invoice::class, 'td_user_id', 'id');
    }


    function engines()
    {

        return $this->hasMany(\App\Engine::class, 'owned_by', 'id');
    }


    public function publishListings()
    {

        // Get Apus ready to publish
        $apus = $this->apus()->where('status', '=', 'draft')->get();

        // Get Engines ready to publish
        $engines = $this->engines()->where('status', '=', 'draft')->get();

        if (!$apus->isEmpty()) {
            $apus->each(function ($apu) {

                $apu->status = "publish";

                $apu->save();
            });
        }


                if (!$engines->isEmpty()) {
            $engine->each(function ($engine) {

                $engine->status = "publish";

                $engine->save();
            });
        }


        // publish all Engines
        $engines = $this->engines()->where('status', '=', 'draft')->get();

        if (!$engines->isEmpty()) {
            $engines->each(function ($engine) {

                $engine->status = "publish";

                $engine->save();
            });
        }
    }

    function hasUnpublished()
    {

        $apus = $this->apus()->where('status', '=', 'draft')->get();
        $engines = $this->engines()->where('status', '=', 'draft')->get();

        if (!$apus->isEmpty() or !$engines->isEmpty()) {
            return true;
        }
    }


    function getUnpublishedApus()
    {

        $apus = $this->apus()->where('status', '=', 'draft')->get();

        if (!$apus->isEmpty()) {
            return $apus;
        }
    }



    function getUnpublishedEngines()
    {
        
        $engines = $this->engines()->where('status', '=', 'draft')->get();

        if (!$engines->isEmpty()) {
            return $engines;
        }
    }

    function unpublishedCount()
    {
        $apus = $this->apus()->where('status', '=', 'draft')->get();
        $engines = $this->engines()->where('status', '=', 'draft')->get();

        $apuCount = $apus->count();
        $enginesCount = $engines->count();

        $count = $apuCount + $enginesCount;

        return $count;
    }

    public function autoRenew()
    {


        // Get APUS

        $apus = $this->apus()
            ->where('status', '=', 'publish')
            ->where('auto_renew', '=', true)
            ->where('days_to_go', '=', 0)
            ->get();

        // Get Engines
        $engines = $this->engines()
            ->where('status', '=', 'publish')
            ->where('auto_renew', '=', true)
            ->where('days_to_go', '=', 0)
            ->get();


        // If user has auto-renew items
        if (!$apus->isEmpty() or !$engines->isEmpty()) {
            if (!$this->isPremium()) {
                // Set API Key
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

                // Get Customer object
                $customer = \Stripe\Customer::retrieve($this->stripe_id);

                // Create empty array for items parameter
                $listItems = [];


                // Create APU Array
                if (!$apus->isEmpty()) {
                    $apuCount = $apus->count(); // get quantity count for Stripe line items.
                    $apuItems =
                        [
                            "type" => "sku",
                            "parent" => config('stripe.apu.sku'),
                            "quantity" => $apuCount,
                            "description" => "APU listing"
                        ];

                    $listItems[] = $apuItems;
                }


                // Create Engine Array

                if (!$engines->isEmpty()) {
                    $engineCount = $engines->count();

                    $engineItems =
                        [
                            "type" => "sku",
                            "parent" => config('stripe.engine.sku'),
                            "quantity" => $engineCount,
                            "description" => "Engine Listing"
                        ];

                    $listItems[] = $engineItems;
                }


                // Create order object
                $order = \Stripe\Order::create([
                        "currency" => "usd",
                        "email" => $this->email,
                        "items" => $listItems,
                    ]);


                // Try the charge
                try {
                    $order->pay([
                        'customer' => $this->stripe_id,

                        'metadata' => [
                            'user_id' => $this->id
                        ]
                    ]);

                    if ($order->status == 'paid') {
                        // reset the 30 days for auto renew.
                        $apus->each(function ($apu) {

                            $apu->days_to_go = 30;
                            $apu->status = "publish";
                            $apu->save();
                        });

                        // reset the 30 days for auto renew.
                        $engines->each(function ($engine) {

                            $engine->days_to_go = 30;
                            $engine->status = "publish";
                            $engine->save();
                        });
                    }
                } catch (Exception $e) {
                    // catch the error and return it
                    $error = $e->getMessage();

                    return $error;
                }
            } else {
                foreach ($apus as $apu) {
                    $apu->renew();
                }

                foreach ($engines as $engine) {
                    $engine->renew();
                }
            }
        }
    }

    // tests whether user is premium

    public function isPremium()
    {

        if ($this->hasStripeId()) {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $customer = \Stripe\Customer::retrieve($this->stripe_id);
            $planId = config('stripe.plan_id');

            if (isset($customer->subscriptions->data[0])) {
                $subscription = $customer->subscriptions->data[0]->plan->id;


                if ($planId == $subscription) {
                    return true;
                }
            }
        }

        return false;
    }

    // Downgrade the user membership.
    public function downgradeMembership()
    {

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = \Stripe\Customer::retrieve($this->stripe_id);
        $planId = config('stripe.plan_id');


        $subscription = $customer->subscriptions->data[0]->id;

        $sub = \Stripe\Subscription::retrieve($subscription);
        $sub->cancel();

        $this->premium = false;
        $this->save();

        if ($sub->status == 'cancelled') {
            return true;
        }
    }

    public function upgradeMembership()
    {

        \Stripe\Stripe::setApiKey("sk_test_QnrOrUxY0S43c6GEhiu9Nwhe");

        $newSubscription = \Stripe\Subscription::create(
            [
                "customer" => $this->stripe_id,
                "plan" => config('stripe.plan_id')
            ]
        );

        if ($newSubscription->status == 'active') {
            $this->premium = true;
            $this->save();
            return true;
        }
    }


    public function plan()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = \Stripe\Customer::retrieve($this->stripe_id);
        $plan = $customer->subscriptions->data[0]->plan;

        return $plan;
    }


    public function subscription()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = \Stripe\Customer::retrieve($this->stripe_id);
        $subscription = $customer->subscriptions->data[0];

        return $subscription;
    }
    
    public function avatar()
    {

        $url = Storage::url('public/avatars/' . $this->id . '/' . $this->id . '.jpg');
        return $url;
    }

    public function hasStripeId()
    {

        //Initialize
        $stripe = new Stripe(env('STRIPE_SECRET'));

        if ($this->stripe_id) {
            $customer = $stripe->customers()->find($this->stripe_id);

            if ($customer) {
                return true;
            }
        } else {
            return false;
        }

//
//        if (!$this->stripe_id == null) {
//
//            return true;
//
//        } else {
//
//            return false;
//        }
    }


    public function hasDefaultCard()
    {

        //Initialize
        $stripe = new Stripe(env('STRIPE_SECRET'));


        if ($this->hasStripeId()) {
            $customer = $stripe->customers()->find($this->stripe_id);

            if (!$customer['default_source'] == null) {
                return true;
            }
        }

        return false;
    }

    public function canPostAircraft()
    {

        if($this->aircraft_approved){

            return true;
        }

        return false;
    }
}
