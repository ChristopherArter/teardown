<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;
use Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $request = app('request');

        if (array_key_exists('daily_email', $data)) {
            $dailyEmail = true;
        } else {
            $dailyEmail = false;
        }

        if (array_key_exists('weekly_email', $data)) {
            $weeklyEmail = true;
        } else {
            $weeklyEmail = false;
        }


        // create stripe id
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET')); // Set API Key
        $newCustomer = \Stripe\Customer::create();
        $stripeID = $newCustomer->id;


        // create user object
        $user = User::create([
            'name' =>$data['name'],
            'email' => $data['email'],
            'stripe_id' => $stripeID,
            'company' => $data['company'],
            'phone_number'=>$data['phone_number'],
            'position'=> $data['position'],
            'daily_email'=>$dailyEmail,
            'weekly_email'=>$weeklyEmail,
            'password' => bcrypt($data['password'])
        ]);

        Storage::makeDirectory('public/avatars/'. $user->id);

        if ($request->hasFile('avatar')) {
            $image = Image::make($request->file('avatar')->getRealPath())->fit(200, 140)->save();

            $path = $request->file('avatar')->storeAs('public/avatars/'. $user->id, $user->id .'.jpg');

            $user->avatar = $path;
        }


        //Initialize
        $stripe = new Stripe(env('STRIPE_SECRET'));

                $newCustomer = $stripe->customers()->create([
                'email' => $user->email,
                'source' => $request->stripeToken,
                'metadata' => ['user_id'=>$user->id]]);





        $user->save();
        
        return $user;
    }
}
