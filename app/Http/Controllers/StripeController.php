<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Message;
use App\User;
use App\InvoiceLineItem;
use Auth;

use App\Notifications\ListingConfirmationNotification;

class StripeController extends Controller
{


    /*
	
	This function intercepts the handle and send it to the right place.

	 */

    public function handleWebHook(Request $request)
    {
    
        $method = 'handle'.studly_case(str_replace('.', '_', $request['type']));


           if (method_exists($this, $method)) {
            return $this->{$method}($request);
        } 

    }



    public function handleInvoicePaymentSucceeded(Request $request)
    {


    	// app('log')->log('critical', $request);
		\Stripe\Stripe::setApiKey(env('STRIPE_SECRET')); // Set API Key


        $amountDue = $request->data['object']['amount_due'];
        $customerId = $request->data['object']['customer'];
        $stripeInvoiceId = $request->data['object']['id'];

        // // Get payment card details
        // $chargeId = $request->data['object']['charge'];
        // $charge = \Stripe\Charge::retrieve($chargeId);
        // $card = \Stripe\Source::retrieve($charge['source']['id'])


        $invoiceDate = $request->data['object']['date'];
        $closed = $request->data['object']['closed'];
        $user = User::where('stripe_id', '=', $customerId)->first();

        
            $invoice = new Invoice();

            $invoice->amount_due = 0;
            $invoice->created_at = $invoiceDate;
            $invoice->stripe_invoice_id = $stripeInvoiceId;
            $invoice->total = $request->data['object']['amount_due'];
            $invoice->td_user_id = $user->id;



            $invoice->status = 'Paid';
            $invoice->save();
            // Set invoice number
            $invoice->invoice_number = $invoice->id + 100;
            $invoice->save();


            /**
             *
             *	Attach some InvoiceLineItems to this thing.
             * 
             */
            

            $getInvoiceItems = \Stripe\Invoice::retrieve($stripeInvoiceId)->lines->all();
            $invoiceItems = $getInvoiceItems['data'];

            	foreach($invoiceItems as $item){



            		$lineItem = new InvoiceLineItem();


            		// If the line item is a plan subscription, the description will be in the meta data. Not sure why but here's an if statement to handle it.
            		if($item['metadata']['description']){
            			$lineItem->description = $item['metadata']['description'];
            		} else {
            			$lineItem->description = $item['description'];
            		}

            		$lineItem->amount = $item['amount'];
            		$lineItem->save();
            		$invoice->lineItems()->save($lineItem);


            	}



            $message = new Message();
            $message->subject = "Thank you for your purchase! Invoice #" . $invoice->invoice_number;
            $message->body = 'Thank you for your purchase! Your invoice is available for download below.<p> <a href="' . route('dashboard.billing.download', $invoice->id) .'"> Download Invoice</a>'; 
            $message->user_id = $user->id;
 			$message->save();
 



 			if(!$request->data['object']['subscription']) {


            // Attach APUs to invoice
            $apus = $user->getUnpublishedApus();
        if (!$apus->isEmpty()) {
            $invoice->apus()->saveMany($apus->all());
            $user->publishListings();
        	}
        
 			

            /**
             *
             *  I need to attach the engines & apus to the invoice models but it's being a little bitch.
             *
             */

            // Attach Engines to invoice
            $engines = $user->getUnpublishedEngines();
            if(!$engines->isEmpty()) {

            	$invoice->engines()->saveMany($engines->all());
            	$user->publishListings();
            
            }
        

			$user->notify(new ListingConfirmationNotification($apus, $engines, $invoice, $user));

			}
            
            $invoice->save();
            $message->save();


           
    }

    public function handleCustomerSubscriptionDeleted(Request $request)
    {

    	// Switch user back to pleb in TD database

        $stripeCustomerId = $request['data']['object']['customer'];
        $user = Auth::user()->where('stripe_id', '=', $stripeCustomerId)->get();
        $user->premium = 0;
        $user->save();

    }
}
