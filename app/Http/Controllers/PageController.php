<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use App\Aircraft;
use App\Engine;
use App\User;
use Auth;
use App\Apu;

class PageController extends Controller
{


/**
 *
 * Show the home page.
 */
    
    public function home(){


        $aircrafts = Aircraft::where('status','=','publish')->take(10)->get();
        $engines = Engine::where('status','=','publish')->take(6)->get();
        $apus = Apu::where('status','=','publish')->take(6)->get();
        $user = Auth::user();



        return view('teardownhome')
            ->with('aircrafts', $aircrafts)
            ->with('engines', $engines)
            ->with('apus', $apus)
            ->with('user', $user);

    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
