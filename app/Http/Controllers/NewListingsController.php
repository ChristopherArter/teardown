<?php

namespace App\Http\Controllers;

use App\Events\ListingsPurchased;
use App\Invoice;
use App\Notifications\ListingConfirmationNotification;
use Illuminate\Http\Request;
use Validator;
use Input;
use Redirect;
use App\Http\Requests;
use App\Apu;
use Auth;
use Carbon\Carbon;
use App\Order;
use App\Message;

use App\Mail\NewListingsPurchaseEmail;

class NewListingsController extends Controller
{
    public function index()
    {

        return view('newlistings.newlistingselect');
    }

    public function apu()
    {

        $apus = Apu::all();
        $engines = Engine::all();
        return view('newlistings.newlistingcreator')
            ->with('type', 'apu')
            ->with('apus', $apus)
            ->with('engines', $engines);
    }

    public function viewCheckout($cart)
    {
    }


    public function processPayment(Request $payment)
    {

        

        // SET VARIABLES
        $user = Auth::user(); // Get User
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET')); // Set API Key
        $customer = \Stripe\Customer::retrieve($user->stripe_id); // Get Customer object

        // Get unpaid APUs
        $apus = $user->apus()->where('status', '=', 'draft')->get();

        // Get unpaid Engines
        $engines = $user->engines()->where('status', '=', 'draft')->get();


        // Create the empty array to fill with lines
        $lines = [];

        if ($user->isPremium()) {
            $apuPrice = 0;
            $enginePrice = 0;
        } else {
            $apuPrice = config('stripe.apu.price');
            $enginePrice = config('stripe.engine.price');
        }





        // Create APU line items
        if (!$apus->isEmpty()) {
            foreach ($apus as $apu) {
                \Stripe\InvoiceItem::create([
                        "customer" => $customer->id,
                        "amount" => $apuPrice,
                        "currency" => "usd",
                        "description" => ''. $apu->model . ' SN ' . $apu->serial . ' Listing']);
            }
        }

        // Create Engine line items
        if (!$engines->isEmpty()) {
            foreach ($engines as $engine) {
                \Stripe\InvoiceItem::create([
                        "customer" => $customer->id,
                        "amount" => $enginePrice,
                        "currency" => "usd",
                        "description" => ''. $engine->model . ' ESN ' . $engine->serial . ' Listing']);
            }
        }


        // // Create invoice here
        // $invoice = \Stripe\Invoice::create(array(
        // 	"customer" => $customer->id
        // ));




/*
*	TRY / CATCH 
	From stripe
*
*
*/




        try {
                // Create invoice here
                $invoice = \Stripe\Invoice::create([
            "customer" => $customer->id
                ]);

                        $invoice->pay();
        } catch (\Stripe\Error\Card $e) {
                  // Since it's a decline, \Stripe\Error\Card will be caught
                  $body = $e->getJsonBody();
                  $err  = $body['error'];

                  print('Status is:' . $e->getHttpStatus() . "\n");
                  print('Type is:' . $err['type'] . "\n");
                  print('Code is:' . $err['code'] . "\n");
                  // param is '' in this case
                  print('Param is:' . $err['param'] . "\n");
                  print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\RateLimit $e) {
                  // Too many requests made to the API too quickly
        } catch (\Stripe\Error\InvalidRequest $e) {
                  // Invalid parameters were supplied to Stripe's API
        } catch (\Stripe\Error\Authentication $e) {
                  // Authentication with Stripe's API failed
                  // (maybe you changed API keys recently)
        } catch (\Stripe\Error\ApiConnection $e) {
                  // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
                  // Display a very generic error to the user, and maybe send
                  // yourself an email
        } catch (Exception $e) {
                  // Something else happened, completely unrelated to Stripe
        }



        return view('newlistings.paymentconfirmation')
            ->with('apus', $apus)
            ->with('engines', $engines);
    }
    
    public function showPaymentForm()
    {



        if (Auth::user()->hasUnpublished()) {
            $total = 0;

            $user = Auth::user();
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $customer = \Stripe\Customer::retrieve($user->stripe_id);

            // APUs
            $apus = Auth::user()->apus()->where('status', '=', 'draft')->get();
            $apuPrice = config('stripe.apu.price');

            $engines = Auth::user()->engines()->where('status', '=', 'draft')->get();
            $enginePrice = config('stripe.engine.price');

            if ($apus) {
                $apuCount = $apus->count();
                $apuTotal = $apuCount * $apuPrice;
            } else {
                $apuTotal = 0;
            }

            if ($engines) {
                $engineCount = $engines->count();
                $engineTotal = $engineCount * $enginePrice;
            } else {
                $engineTotal = 0;
            }

            $total = $apuTotal + $engineTotal;

            $final = $total / 100;

            // find out if user is premium, if so return this
            return view('newlistings.paymentform')
                ->with('premium', false)
                ->with('apus', $apus)
                ->with('engines', $engines)
                ->with('customer', $customer)
                ->with('apuPrice', $apuPrice)
                ->with('total', $final)
                ->with('error', 'buttsecks');
        } else {
            return redirect()->back();
        }
    }

    public function renewListing(Request $request)
    {

        // SET VARIABLES
        $user = Auth::user(); // Get User
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET')); // Set API Key
        $customer = \Stripe\Customer::retrieve($user->stripe_id); // Get Customer object

        $listing = $model->id;

        $order =   \Stripe\Order::create([
                "currency" => "usd",
                "email" => $user->email,
                "items" => [
                    "type" => "sku",
                    "parent" => $request->sku,
                    "quantity"=> $request->quantity,
                    "description" => "Listing Renewal"
                ],
            ]);


        $listing->renew();
    }
}
