<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Input;
use Redirect;
use App\Http\Requests;
use App\Apu;
use Auth;
use Carbon\Carbon;

class ApuController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * (shows all posts)
     */




    public function index()
    {
        $apus = Apu::where('status', '=', 'publish')->get();
        return view('APUs.index')->with('apus', $apus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // pass "POST" method to the form creator to change form POST / PUT. getting an invalid method coming into the APU controller.


        $user = Auth::user();
        $apus = $user->apus()->where('status', '=', 'draft')->get();
        $editApu = new Apu();
        return view('newlistings.newlistingcreator')
            ->with('type', 'apu')
            ->with('apus', $apus)
            ->with('editApu', $editApu)
            ->with('form', 'edit')
            ->with('user', $user)
            ->with('Apu', $editApu)
            ->with('status', 'draft')
            ->with('method', 'post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * processes the form
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $editApu = new Apu;


        $validator = Validator::make($request->all(), [
            'part_number' => 'required|max:255',
            'serial' => 'required|max:255',
            'model' => 'required|max:255',
            'tagged_by' => 'max:255|required_if:condition,OH|required_if:condition,SV|required_if:condition,NE',
            'csn'=> 'numeric',
            'tsn'=> 'numeric',
            'tslsv'=> 'numeric',
            'cslsv'=> 'numeric',
            'condition'=>'required',
            'minipack'=>'url',
            'tag_date'=>'date:m/d/Y'
        ]);

        if ($validator->fails()) {
            return redirect()->route('apus.create')
                ->with('type', 'apu')
                ->with('form', 'edit')
                ->with('Apu', $editApu)
                ->with('method', 'post')
                ->with('user', $user)
                ->withErrors($validator)
                ->withInput()
                ->with('editApu', $editApu);
        }


        // BASIC DETAILS
        $editApu->part_number = $request->input('part_number'); // Part number
        $editApu->serial = $request->input('serial'); // Serial number
        $editApu->model = $request->input('model'); // Model number
        $editApu->condition = $request->input('condition'); // condition
        $editApu->owned_by = Auth::user()->id; // Set owner to current logged in user


        if ($editApu->condition == 'AR') {
            $editApu->tagged_by = null;
            $editApu->details = null;
            $editApu->tag_date = null;
        } else {
            // TAG INFO
            $editApu->tagged_by = $request->input('tagged_by'); // Tagged by
            $editApu->details = $request->input('details'); // 8130 Details
            $editApu->tag_date = $request->input('tag_date');
        }

        // TRACE
        $editApu->trace = $request->input('trace');

        // TIMES & CYCLES
        $editApu->tsn = $request->input('tsn'); // Time Since New
        $editApu->csn = $request->input('csn'); // Cycles Since new
        $editApu->tslsv = $request->input('tslsv'); // Time Since Last Shop Visit
        $editApu->cslsv = $request->input('cslsv'); // Cycles Since Last Shop Visit

        // MINIPACK
        $editApu->minipack = $request->input('minipack');


        $editApu->sale = $request->input('sale');
        $editApu->lease = $request->input('lease');
        $editApu->exchange = $request->input('exchange');
        $editApu->updated_at = Carbon::now();
        $editApu->status = "draft";

        $editApu->auto_renew = $request->input('auto_renew');


        // Save to the database
        $editApu->save();
        $request->session()->flash('alert-success', 'Successfully saved!');

        // Redirect to edit view
        return redirect()->route('apus.edit', $editApu->id)
            ->with('type', 'apu')
            ->with('editApu', $editApu)
            ->with('form', 'edit')
            ->with('user', $user)
            ->with('Apu', $editApu)
            ->with('method', 'put');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $apu = Apu::find($id);
        if ($apu->expired()) {
            return redirect()->route('home');
        } else {
            return view('apus.single')->with('apu', $apu);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = Auth::user();
        $editApu = Apu::find($id);
        $status = $editApu->status;

        return view('newlistings.newlistingcreator')->with('type', 'apu')
            ->with('form', 'edit')
            ->with('editApu', $editApu)
            ->with('listing', $editApu)
            ->with('Apu', $editApu)
            ->with('user', $user)
            ->with('status', $status)
            ->with('method', 'put');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editApu = Apu::find($id);
        // VALIDATION
        $user = Auth::user();
        $request->flash();


        $validator = Validator::make(
            $request->all(),
            [
                'part_number' => 'required|max:255',
                'serial' => 'required|max:255',
                'model' => 'required|max:255',
                'tagged_by' => 'max:255|required_if:condition,OH|required_if:condition,SV|required_if:condition,NE',
                'csn'=> 'numeric',
                'tsn'=> 'numeric',
                'tslsv'=> 'numeric',
                'cslsv'=> 'numeric',
                'condition'=>'required',
                'tag_date'=>'date:m/d/Y|required_if:condition,OH|required_if:condition,SV|required_if:condition,NE'
            ]
        );

        if ($validator->fails()) {
            return redirect('apus/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput()->with('editApu', $editApu)
            ->with('method', 'put');
        }


       if(! $editApu->status == 'publish') {

        // BASIC DETAILS
        $editApu->part_number = $request->input('part_number'); // Part number
        $editApu->serial = $request->input('serial'); // Serial number
        $editApu->model = $request->input('model'); // Model number
        $editApu->condition = $request->input('condition'); // condition

    }
    
        $editApu->owned_by = Auth::user()->id; // Set owner to current logged in user


        if ($editApu->condition == 'AR') {
            $editApu->tagged_by = null;
            $editApu->details = null;
            $editApu->tag_date = null;
        } else {
            // TAG INFO
            $editApu->tagged_by = $request->input('tagged_by'); // Tagged by
            $editApu->details = $request->input('details'); // 8130 Details
            $editApu->tag_date = $request->input('tag_date');
        }

        // TRACE
        $editApu->trace = $request->input('trace');

        // TIMES & CYCLES
        $editApu->tsn = $request->input('tsn'); // Time Since New
        $editApu->csn = $request->input('csn'); // Cycles Since new
        $editApu->tslsv = $request->input('tslsv'); // Time Since Last Shop Visit
        $editApu->cslsv = $request->input('cslsv'); // Cycles Since Last Shop Visit

        $editApu->comments = $request->input('comments'); // Additional comments

        // MINIPACK
        $editApu->minipack = $request->input('minipack');


        $editApu->sale = $request->input('sale');
        $editApu->lease = $request->input('lease');
        $editApu->exchange = $request->input('exchange');
        $editApu->auto_renew = $request->input('auto_renew');

        $editApu->updated_at = Carbon::now();

//        $editApu->status = "draft";
        $editApu->save();

        return redirect()->route('apus.edit', $editApu->id)
            ->with('type', 'apu')
            ->with('editApu', $editApu)
            ->with('form', 'edit')
            ->with('Apu', $editApu)
            ->with('user', $user)
            ->with('method', 'put');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Apu::find($id);
        $destroy->status = 'cancelled';
        $destroy->save();
        return redirect()->route('apus.create');
    }

    public function softDestroy($id)
    {
        $destroy = Apu::find($id);
        $destroy->status = 'cancelled';
        $destroy->save();
    }
}
