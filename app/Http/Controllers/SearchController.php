<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Apu;
use App\Engine;
use App\Aircraft;
use App\MarketPost;
use DB;

class SearchController extends Controller
{


    public function search(Request $request)
    {

        $term = $request->input('term');




        if (!$term == null) {
            // APU Query
            $apus = Apu::where('model', 'like', '%' . $term . '%')
                ->where('status', '=', 'publish')
                ->orWhere('serial', 'like', '%' . $term . '%')
                ->orWhere('part_number', 'like', '%' . $term . '%')
                ->get();

            // Engine Query
            $engines = Engine::where('model', 'like', '%' . $term . '%')
                ->where('status', '=', 'publish')
                ->orWhere('serial', 'like', '%' . $term . '%')
                ->orWhere('minor_variant', 'like', '%' . $term . '%')
                ->orWhere('part_number', 'like', '%' . $term . '%')
                ->get();

            // Aircraft Query

            $aircrafts = DB::table('aircraft')->where('model', 'like', '%' . $term . '%')
                ->orWhere('serial', 'like', '%' . $term . '%')
                ->orWhere('tail_number', 'like', '%' . $term . '%')
                ->orWhere('ex_airline', 'like', '%' . $term . '%')
                ->get();

            // Market Query
            $marketPosts = DB::table('market_posts')->where('market_title', 'like', '%' . $term . '%')
                ->orWhere('market_body', 'like', '%' . $term . '%')
                ->get();

            return view('search.search')
                ->with('apus', $apus)
                ->with('engines', $engines)
                ->with('marketPosts', $marketPosts)
                ->with('aircrafts', $aircrafts)
                ->with('term', $term);
        } else {
            return redirect()->back();
        }
    }
}
