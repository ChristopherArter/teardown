<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MarketPost;
use App\Http\Controllers\Controller;
use App\Favorite;

class MarketPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//
//        $marketPosts = MarketPost::orderBy('id', 'desc')->get();
//        $marketCategories = config('app.market_categories');
//        $marketPostTypes = config('app.market_post_types');
//
//        return view('market.marketmaster')->with('marketPosts', $marketPosts)
//            ->with('marketCategories', $marketCategories)
//            ->with('marketPostTypes', $marketPostTypes);
//    }

    public function index()
    {

                $marketPosts = MarketPost::orderBy('id', 'desc')->get();
        $marketCategories = config('app.market_categories');
        $marketPostTypes = config('app.market_post_types');

        return view('market.marketmaster2')->with('marketPosts', $marketPosts)
            ->with('marketCategories', $marketCategories)
            ->with('marketPostTypes', $marketPostTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $apus = Apu::all();
        return view('newlistings.newlistingcreator')->with('type', 'apu')->with('apus', $apus);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Set variables
        $marketCategories = implode(',', config('app.market_categories'));


       // Validate input
        $this->validate($request, [
        'market_title'=>'required|max:80',
          'market_body'=>'required',
          'market_cat'=>'required|in:' . $marketCategories
        ]);

      // Store in database
        $marketPost = new MarketPost;

        $marketPost->market_title = $request->market_title;
        $marketPost->market_body = $request->market_body;
        $now = new \DateTime();
        $marketPost->created_at = $now;
        $marketPost->market_cat = $request->market_cat;
        $marketPost->market_type = $request->market_type;

        $marketPost->save();


      // Redirect
        return redirect()->route('market.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = MarketPost::find($id);

        $post->delete();

        return redirect()->back();
    }
    /**
     * Return the view of market posts based on parametric form
     *
     * @param  Request
     * @return \Illuminate\Http\Response
     */
    public function marketAjax(Request $request)
    {
        if ($request->ajax()) {
            $postTypesArray = $request->postTypes;
            $categoriesArray = $request->categories;
            $keyword = $request->keyword;
            $marketPosts = MarketPost::orderBy('id', 'desc')->whereIn('market_type', $postTypesArray)
                ->whereIn('market_cat', $categoriesArray)
                ->when($keyword, function ($query) use ($keyword) {

                    return $query->where('market_body', 'like', '%' . $keyword . '%')
                        ->orWhere('market_title', 'like', '%' . $keyword . '%');
                })
                ->get();

//    $this->validate($request, array(
//
//
//    ));

            return view('includes.market._marketcontent')->with('marketPosts', $marketPosts);
        }
    }
}
