<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use App\Engine;
use Auth;
use Carbon\Carbon;

class EngineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $engines = Engine::where('status', '=', 'publish')->get();
        return view('Engines.index')->with('engines', $engines);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // pass "POST" method to the form creator to change form POST / PUT. getting an invalid method coming into the APU controller.
        $user = Auth::user();
        $engines = $user->engines()->where('status', '=', 'draft')->get();
        $editEngine = new Engine();
        return view('newlistings.newlistingcreator')
            ->with('type', 'engine')
            ->with('editEngine', $editEngine)
            ->with('form', 'edit')
            ->with('user', $user)
            ->with('Engine', $editEngine)
            ->with('status', 'draft')
            ->with('method', 'post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $user = Auth::user();
        $editEngine = new Engine;


        $validator = Validator::make($request->all(), [
            'part_number' => 'required|max:255',
            'serial' => 'required|max:255',
            'model' => 'required|max:255',
            'tagged_by' => 'max:255|required_if:condition,OH|required_if:condition,SV|required_if:condition,NE',
            'csn'=> 'numeric',
            'tsn'=> 'numeric',
            'tslsv'=> 'numeric',
            'cslsv'=> 'numeric',
            'condition'=>'required',
            'tag_date'=>'date:m/d/Y'
        ]);

        if ($validator->fails()) {
            return redirect()->route("engines.create")
             ->with('type', 'engine')
            ->with('editEngine', $editEngine)
            ->with('form', 'edit')
            ->with('user', $user)
                ->withErrors($validator)
            ->with('Engine', $editEngine)
            ->with('method', 'post');
        }


 

        // BASIC DETAILS
        $editEngine->part_number = $request->input('part_number'); // Part number
        $editEngine->serial = $request->input('serial'); // Serial number
        $editEngine->model = $request->input('model'); // Model number
        $editEngine->minor_variant = $request->input('minor_variant'); // Minor variant
        $editEngine->condition = $request->input('condition'); // condition


        $editEngine->owned_by = Auth::user()->id; // Set owner to current logged in user


        if ($editEngine->condition == 'AR') {
            $editEngine->tagged_by = null;
            $editEngine->details = null;
            $editEngine->tag_date = null;
        } else {
            // TAG INFO
            $editEngine->tagged_by = $request->input('tagged_by'); // Tagged by
            $editEngine->details = $request->input('details'); // 8130 Details
            $editEngine->tag_date = $request->input('tag_date');
        }

        // TRACE
        $editEngine->trace = $request->input('trace');

        // TIMES & CYCLES
        $editEngine->tsn = $request->input('tsn'); // Time Since New
        $editEngine->csn = $request->input('csn'); // Cycles Since new
        $editEngine->tslsv = $request->input('tslsv'); // Time Since Last Shop Visit
        $editEngine->cslsv = $request->input('cslsv'); // Cycles Since Last Shop Visit

        // MINIPACK
        $editEngine->minipack = $request->input('minipack');



        $editEngine->sale = $request->input('sale');
        $editEngine->lease = $request->input('lease');
        $editEngine->exchange = $request->input('exchange');
        $editEngine->updated_at = Carbon::now();
        $editEngine->status = "draft";


        // Save to the database
        $editEngine->save();
        $request->session()->flash('alert-success', 'Successfully saved!');

        // Redirect to edit view
        return redirect()->route('engines.edit', $editEngine->id)
            ->with('type', "engine")
            ->with("editEngine", $editEngine)
            ->with('form', 'edit')
            ->with('user', $user)
            ->with("Engine", $editEngine)
            ->with('method', 'put');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $editEngine = Engine::find($id);
        $status = $editEngine->status;


        return view('newlistings.newlistingcreator')->with('type', 'engine')
            ->with('form', 'edit')
            ->with('editEngine', $editEngine)
            ->with('Engine', $editEngine)
            ->with('user', $user)
            ->with('listing', $editEngine)
            ->with('status', $status)
            ->with('method', 'put');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editEngine = Engine::find($id);
        // VALIDATION
        $user = Auth::user();
        $request->flash();


        $validator = Validator::make(
            $request->all(),
            [
                'part_number' => 'required|max:255',
                'serial' => 'required|max:255',
                'model' => 'required|max:255',
                'tagged_by' => 'max:255|required_if:condition,OH|required_if:condition,SV|required_if:condition,NE',
                'csn'=> 'numeric',
                'tsn'=> 'numeric',
                'tslsv'=> 'numeric',
                'cslsv'=> 'numeric',
                'condition'=>'required',
                'tag_date'=>'date:m/d/Y|required_if:condition,OH|required_if:condition,SV|required_if:condition,NE'
            ]
        );

        if ($validator->fails()) {
            return redirect('engines/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput()->with('editEngine', $editEngine)
                ->with('method', 'put');
        }


       if(! $editEngine->status == 'publish') {

        // BASIC DETAILS
        $editEngine->part_number = $request->input('part_number'); // Part number
        $editEngine->serial = $request->input('serial'); // Serial number
        $editEngine->model = $request->input('model'); // Model number
        $editEngine->minor_variant = $request->input('minor_variant'); // Minor variant

}

        $editEngine->condition = $request->input('condition'); // condition
        $editEngine->owned_by = Auth::user()->id; // Set owner to current logged in user


        if ($editEngine->condition == 'AR') {
            $editEngine->tagged_by = null;
            $editEngine->details = null;
            $editEngine->tag_date = null;
        } else {
            // TAG INFO
            $editEngine->tagged_by = $request->input('tagged_by'); // Tagged by
            $editEngine->details = $request->input('details'); // 8130 Details
            $editEngine->tag_date = $request->input('tag_date');
        }

        // TRACE
        $editEngine->trace = $request->input('trace');

        // TIMES & CYCLES
        $editEngine->tsn = $request->input('tsn'); // Time Since New
        $editEngine->csn = $request->input('csn'); // Cycles Since new
        $editEngine->tslsv = $request->input('tslsv'); // Time Since Last Shop Visit
        $editEngine->cslsv = $request->input('cslsv'); // Cycles Since Last Shop Visit

        // MINIPACK
        $editEngine->minipack = $request->input('minipack');


        $editEngine->sale = $request->input('sale');
        $editEngine->lease = $request->input('lease');
        $editEngine->exchange = $request->input('exchange');

        $editEngine->updated_at = Carbon::now();

//        $editEngine->status = "draft";
        $editEngine->save();

        return redirect()->route("engines.edit", $editEngine->id)
            ->with('type', "engine")
            ->with('editEngine', $editEngine)
            ->with('form', 'edit')
            ->with('Engine', $editEngine)
            ->with('user', $user)
            ->with('method', 'put');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Engine::find($id);
        $destroy->status = 'cancelled';
        $destroy->save();
        return redirect()->back();
    }
}
