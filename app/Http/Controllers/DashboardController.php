<?php

namespace App\Http\Controllers;

use App\Apu;
use App\Invoice;
use App\User;
use App\Engine;
use App\MarketPost;
use Illuminate\Http\Request;
use Validator;
use Input;
use Redirect;
use Auth;
use App\Order;
use DB;
use Illuminate\Http\Response;
use Cartalyst\Stripe\Stripe;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Http\Requests;
use Illuminate\Support\Facades\View;
use App\Message;
use App\ActiveSearch;
use Illuminate\Support\Facades\Cache;

use Mailgun\Mailgun;

class DashboardController extends Controller
{



    public function index()
    {
//        $stripe = new Stripe(env('STRIPE_SECRET'));

        $user = Auth::user();

//        $customer = $stripe->customers()->find($user->stripe_id);
        return view('dashboard.profile')->with('user', $user);
    }





    public function billing()
    {

        /*
         *
         *  PAYMENT OPTIONS MANAGEMENT
         *
         */


        // Get user object
        $user = Auth::user();

        // If user has stripe id already
        if ($user->hasStripeId() and $user->hasDefaultCard()) {
            //Initialize
            $stripe = new Stripe(env('STRIPE_SECRET'));


            // Retreive all cards
            $cards = $stripe->cards()->all($user->stripe_id);
            $customer = $stripe->customers()->find($user->stripe_id);

            // Return view with $card array
            return view('dashboard.billing')->with('cards', $cards)->with('customer', $customer)->with('hasStripe', true);
        }


        // Return view with $card array
        return view('dashboard.billing')->with('hasStripe', false);
    }






    function addCreditCard(Request $request)
    {

        //Initialize
        $stripe = new Stripe(env('STRIPE_SECRET'));

        // Get user object
        $user = Auth::user();
        
        
        
        if ($user->hasStripeId()) {
            $card = $stripe->cards()->create($user->stripe_id, $request->stripeToken);

            if ($request->session()->has('inbound_url')) {
                return redirect($request->session()->get('inbound_url'));
            } else {
                return redirect()->back();
            }
        }

            $user->stripe_id = $newCustomer['id'];

            $user->save();


        if ($request->session()->has('inbound_url')) {
            return redirect($request->session()->get('inbound_url'));
        } else {
            return redirect()->back();
        }
    }


    

    function setNewDefault(Request $request)
    {

        $user = Auth::user();
        // load in the data

        // Get user's stripe object
        \Stripe\Stripe::setApiKey("sk_test_QnrOrUxY0S43c6GEhiu9Nwhe");

        $customer = \Stripe\Customer::retrieve($user->stripe_id);
        $customer->default_source = $request->newDefault;
        $customer->save();

        return redirect('/dashboard/billing');
    }

    function deleteCard(Request $request)
    {

        $user = Auth::user();
        // load in the data

        // Get user's stripe object
        \Stripe\Stripe::setApiKey("sk_test_QnrOrUxY0S43c6GEhiu9Nwhe");

        $customer = \Stripe\Customer::retrieve($user->stripe_id);
        $customer->sources->retrieve($request->deleteID)->delete();

        return redirect('/dashboard/billing');
    }

    public function invoices()
    {

        /*
         *
         *  Show invoices
         *
         */

        // Get user object
        $user = Auth::user();

        $invoices = $user->invoices()->latest()->get();


        if (!$invoices== null) {
            return view('dashboard.invoices')
                ->with('invoices', $invoices)
                ->with('user', $user);
        } else {
            return view('dashboard.invoices')
            ->with('user', $user);
        }
    }


    public function listings()
    {

        /*
         *
         * Manage Listings
         *
         */

        $user = Auth::user();

        $engines = $user->engines()->where('days_to_go', '>', 0)->get();
        $apus = $user->apus()->get();
        $marketPosts = $user->marketPosts()->take(10)->get();
        return view('dashboard.listings')

            ->with('engines', $engines)
            ->with('apus', $apus)
            ->with('marketPosts', $marketPosts);
    }

    public function profile()
    {

        /*
         *
         * Show Profile Page
         *
         */

        $user = Auth::user();
        return view('dashboard.profile')->with('user', $user);
    }

    public function updateUser(Request $request)
    {

        /*
		 *
		 * Used to update user at profile page
		 *
		 */
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'avatar' => 'image|mimes:jpeg,jpg'
        ]);
        if ($validator->fails()) {
            return redirect()->back();
        }


        $user = Auth::user();

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone_number = $request->input('phone_number');


        /*
         * Email list management with mailgun
         */



        # Instantiate the client.
        $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        $listMember = $user->email;
        $memberAddress = $user->email;
        $dailyAddress = env('MAILGUN_DAILY_ADDRESS');
        $weeklyAddress = env('MAILGUN_WEEKLY_ADDRESS');

        
        if ($request->input('daily_email')) {
            // add user to mailing list

            $result = $mgClient->post("lists/$dailyAddress/members", [
                'address' => $user->email,
                'name' => $user->name,
                'subscribed' => true,
                'upsert' => 'yes'
            ]);
        } elseif (!$request->input('daily_email')) {
            if ($user->daily_email == true) {
                $result = $mgClient->delete("lists/$dailyAddress/members/$listMember");
            }
        }


        if ($request->input('weekly_email')) {
            $result = $mgClient->post("lists/$weeklyAddress/members", [
            'address' => $user->email,
            'name' => $user->name,
            'subscribed' => true,
            'upsert' => 'yes'
            ]);
        } elseif (!$request->input('weekly_email')) {
            if ($user->weekly_email == true) {
                $result = $mgClient->delete("lists/$weeklyAddress/members/$listMember");
            }
        }

            // DAILY EMAIL LIST
            $user->daily_email = $request->input('daily_email');

            // WEEKLY EMAIL LIST
            $user->weekly_email = $request->input('weekly_email');


            $user->position = $request->input('position');
            $user->company = $request->input('company');


            // AVATAR
        if ($request->hasFile('avatar')) {
            $directory = 'public/avatars/' . $request->user()->id . '/';

              

               
            $files = Storage::files($directory);
            Storage::delete($files);

            $image = Image::make($request->file('avatar'))->fit(200, 140)->save();
            $path = Storage::put('public/avatars/' . $request->user()->id . '/' . $request->user()->id . '.jpg', $image);

            $user->avatar = $path;
        }

            $user->save();
            Cache::flush();

            return View::make('dashboard.profile')->with('user', $user)->render();
    }

    

    public function removeAvatar(Request $request)
    {

        $user = Auth::user();

        $user->avatar = null;

        $user->save();

        Storage::delete('public/avatars/' . $user->id . '/' . $user->id . '.jpg');

        return redirect()->back();
    }


    public function messages()
    {
        /*
         *
         * Show messages inbox (called "notifications" for now)
         *
         */
        $user = Auth::user();



        $msgs = $user->messages()->latest()->paginate(15);

        // $messages = Message::where('user_id', '==', $user->id)->paginate(15);



        return view('dashboard.messages')
                ->with('msgs', $msgs);
    }

    public function membership()
    {
        /*
         *
         * Show membership panel
         *
         */


        //Initialize
        $stripe = new Stripe(env('STRIPE_SECRET'));

        // Get user object
        $user = Auth::user();

        if ($user->hasStripeId()) {
            $customer = $stripe->customers()->find($user->stripe_id);

            $subscription = $customer['subscriptions'];

            if ($user->isPremium()) {
                return view('dashboard.membership')->with('sub', $subscription['data']['0']);
            } else {
                return view('dashboard.membership');
            }
        } else {
            return view('dashboard.membership');
        }
    }




    public function activesearch()
    {


        $user = Auth::user();
        $searches = $user->activeSearches()->get();
        $ActiveSearch = ActiveSearch::find(1);

        return view('dashboard.activesearch')->with('ActiveSearch', $ActiveSearch);
    }


    public function showUpgrade()
    {
        /*
         *
         * Show the upgrade confirmation page
         *
         */


        // Get user's stripe object
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        // Get user object
        $user = Auth::user();

        if (!$user->isPremium()) {
            $customer = \Stripe\Customer::retrieve($user->stripe_id);

            return view('dashboard.membershipupgrade')->with('customer', $customer)->with('total', 49999);
        } else {
            return redirect()->route('dashboard.membership');
        }
    }

    public function processUpgrade(Request $request)
    {
        /*
         *
         * Process the membership upgrade
         * This thing is a cluster fuck. I really need to refactor this.
         */

        //Initialize
        $stripe = new Stripe(env('STRIPE_SECRET'));

        // Get user object
        $user = Auth::user();

        $planID = config('stripe.plan_id');

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        // Add the membership on there

        $subscription = $stripe->subscriptions()->create($user->stripe_id, [
            'plan' => $planID,
            'metadata'=> ['description'=>'Teardown.AERO Premium Membership']
        ]);


        $membership_invoice = \Stripe\Invoice::all([

            "limit" => 1,
            "customer"=>$user->stripe_id

        ]);


        $retreiveInvoice = \Stripe\Invoice::retrieve($membership_invoice->data[0]['id']);


        $retreiveInvoice->lines->data[0]['description'] = "Teardown.AERO Premium Membership";
        $retreiveInvoice->save();


        return redirect()->route('dashboard.membership');
    }

    public function cancelSubscription(Request $request)
    {
        /*
         *
         * Handle cancel subscription request
         *
         */

        //Initialize
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        // Get user object
        $user = Auth::user();

        if ($user->isPremium()) {
            $customer = \Stripe\Customer::retrieve($user->stripe_id);
            $subscription = $customer->subscriptions->data[0]->id;

            $sub = \Stripe\Subscription::retrieve($subscription);
            $sub->cancel();
        }

        return redirect()->route('dashboard.membership');
    }
}
