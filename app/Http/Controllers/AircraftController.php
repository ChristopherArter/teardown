<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Aircraft;
class AircraftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $aircrafts = Aircraft::all();
      
        return $aircrafts;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aircrafts.aircraftscreate')->with('method','post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validation
        // 
        $this->validate($request, [
        'make' => 'required|max:255',
        'model' => 'required|max:255',
        'serial' => 'required|max:255',
        'tail_number' => 'required|max:255',
        'ex_airline' => 'required|max:255',
    ]);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $aircraft = Aircraft::find($id);

       if($aircraft->isPublished()){

        return view('aircrafts.aircraftsingle')->with('aircraft', $aircraft);

       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $aircraft = Aircraft::find($id);

       return view('aircrafts.aircraftscreate')->with('method','put')->with('aircraft', $aircraft);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

        public function __construct()
    {
        
    $this->middleware('check_aircraft_approved', ['except' => ['index','show']]);
    }
}
