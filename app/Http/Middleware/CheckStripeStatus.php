<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Cartalyst\Stripe\Stripe;
use App\User;

class CheckStripeStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user->hasStripeId() and $user->hasDefaultCard()) {
            return $next($request);
        } else {
            $request->session()->put('inbound_url', $request->url());
            $request->session()->flash('inbound_url_message', 'Please add a payment method before making a purchase.');

            return redirect()->route('dashboard.billing');
        }
    }
}
