<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;
class CheckAircraftApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


    $user = Auth::user();

    if ($user->aircraft_approved == true){

        return $next($request);

    } else {

            return redirect()->back();

    }

    }
}
