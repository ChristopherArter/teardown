<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $table = 'messages';
    

    public function user()
    {

        return $this->belongsTo(\App\User::class, 'id', 'user_id');
    }
}
