<?php

namespace App\Console;

use DB;
use App\Apu;
use App\MarketPost;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\DailyCountdown::class,
        Commands\DailyEmail::class,
        Commands\WeeklyEmailCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */

    // These times are in UTC.. so account for the -5 hour difference between UTC and ETC
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('listings:DailyCountdown')->dailyAt('05:00');
        $schedule->command('listings:DailyEmail')->dailyAt('14:15');
        $schedule->command('listings:WeeklyEmail')->weekly()->mondays()->at('14:15');
    }
    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
