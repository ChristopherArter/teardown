<?php

namespace App\Console\Commands;

use App\Mail\DailyListings;
use Illuminate\Support\Facades\Mail;
use App\User;
use Auth;
use App\Apu;
use App\Engine;
use App\Aircraft;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Console\Command;
use App\Events\DailyEmailEvent;
use App\Notifications\ExpirationWarning;
use Illuminate\Support\Facades\Log;

class DailyEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listings:DailyEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends an email with the current listings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        /*
		 *
		 * 			If there are new listings for the day, send out an email. Otherwise, don't.
		 *
		 *
		 */

        $apus = DB::table('apus')->select(DB::raw('*'))
            ->whereRaw('Date(created_at) = CURDATE()')->get();

        $engines = DB::table('engines')->select(DB::raw('*'))
            ->whereRaw('Date(created_at) = CURDATE()')->get();

        $marketPosts = DB::table('market_posts')->select(DB::raw('*'))
            ->whereRaw('Date(created_at) = CURDATE()')->get();

//		$aircrafts = DB::table('aircraft')->get();

        if (!$apus->isEmpty() or !$engines->isEmpty() or !$marketPosts->isEmpty()) {
            Mail::to('christopherarter@gmail.com')->send(new DailyListings($apus, $engines, $marketPosts));
        }











        /*
		 *
		 * 			Do a daily check for users who have listings expiring soon.
		 *
		 *
		 *
		 */
        
        // Access all users
        $users = User::all();

        foreach ($users as $user) {
            $apuListings = $user->apus()->where('days_to_go', '=', 3)->where('status', '=', 'publish')->where('auto_renew', '=', false)->get();
            $engineListings = $user->engines()->where('days_to_go', '=', 3)->where('status', '=', 'publish')->where('auto_renew', '=', false)->get();

            if (!$apuListings->isEmpty() or !$engineListings->isEmpty()) {
                $user->notify(new ExpirationWarning($apuListings, $engineListings, $user));
            }
        }
    }
}
