<?php

namespace App\Console\Commands;

use App\Apu;
use App\Engine;
use App\Events\DailyTasks;

use Auth;
use DB;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class DailyCountdown extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listings:DailyCountdown';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Decrements "days_to_go" column in all premium listing tables. This runs every 24 hours at midnight Eastern US time.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * This task decrements the "days_to_go" column at midnight each day.
     *
     * @return mixed
     */
    public function handle()
    {

        // Fire daily event.
        event(new DailyTasks());

//        $users = User::all();
//
//        foreach ($users as $user)
//        {
//
//        }
    }
}
