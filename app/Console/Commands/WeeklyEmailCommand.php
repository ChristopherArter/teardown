<?php

namespace App\Console\Commands;

use App\Mail\DailyListings;
use App\Mail\WeeklyEmail;
use App\Notifications\ExpirationWarning;
use Illuminate\Support\Facades\Mail;
use App\Apu;
use Auth;
use App\User;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Console\Command;

class WeeklyEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listings:weeklyemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weekly emails of listings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    
        $user = Auth::user();
    
        $user->notify(new ExpirationWarning($user));

        // Change this when it goes into production.
//        Mail::to('weekly@sandboxdebfc32f503d450184a8c25d5f7d4bcf.mailgun.org')->send(new WeeklyEmail());

//        Mail::to('christopherarter@gmail.com')->send(new DailyListings());
    }
}
