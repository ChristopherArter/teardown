<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
use App\Apu;
use App\Engine;
use App\User;

class ExpiringSoonEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $apuListings;
    public $engineListings;

    public function __construct($apuListings, $engineListings)
    {
        $this->apuListings = $apuListings;
        $this->engineListings = $engineListings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $user = Auth::user();

        return $this->from('info@teardown.aero')
            ->view('mail.expiringsoon')
            ->with('apuListings', $this->apuListings)
            ->with('engineListings', $this->engineListings);
    }
}
