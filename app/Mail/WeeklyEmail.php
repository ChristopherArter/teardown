<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Apu;

class WeeklyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $apus = Apu::where('status', '=', 'publish')->get();
        return $this->from('christopherarter@gmail.com')->subject('New Teardown.AERO Listings')
            ->view('mail.dailylistings')->with('apus', $apus);
    }
}
