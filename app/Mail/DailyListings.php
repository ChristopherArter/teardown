<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class DailyListings extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $apus;
    public $engines;
    public $marketPosts;
//    public $aircrafts;


    public function __construct($apus, $engines, $marketPosts)
    {
        $this->apus = $apus;
        $this->engines = $engines;
        $this->marketPosts = $marketPosts;
//        $this->aircrafts = $aircrafts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        

        return $this->from('christopherarter@gmail.com')->subject('Your Daily Teardown.AERO Listings')
            ->view('mail.dailylistings')
            ->with('apus', $this->apus)
            ->with('engines', $this->engines)
            ->with('marketPosts', $this->marketPosts);
    }
}
