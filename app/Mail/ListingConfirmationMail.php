<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ListingConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $apus;
    public $engines;
    public $invoice;

    public function __construct($apus, $engines, $invoice)
    {

        $this->apus = $apus;
        $this->engines = $engines;
        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.listingconfirmation')
            ->from('info@teardown.aero')
            ->subject('New Listing Confirmation')
            ->with('apuListings', $this->apus)
            ->with('engineListings', $this->engines)
            ->with('invoice', $this->invoice);
    }
}
