<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Apu;
use App\Engine;
use App\Aircraft;
use App\ActiveSearch;
use App\User;
use App\SearchParameter;
use DB;

class ActiveSearch extends Model
{

    public $timestamps = false;
    protected $casts = [
        'params' => 'array'
    ];

    protected $table = 'active_searches';

    public function user()
    {

        return $this->belongsTo(\App\User::class, 'id', 'user_id');
    }


    public function apus()
    {
    }

//	public function searchParameters(){
//
//		return $this->hasMany('App\SearchParameter', 'as_id', 'id');
//	}

/*
 *
 * Not gonna lie.. this method is pretty bad ass.
 *
 */
    public function search($model)
    {

        $params = $this->params;

        $results = $model->where('status', '=', 'publish')
        ->where(function ($query) use ($params) {

            foreach ($params as $key => $value) {
                $query->where($key, '=', $value);
            }
        })
        ->get();

        if (!$results->isEmpty()) {
            return $results;
        } else {
            return null;
        }
    }
}
