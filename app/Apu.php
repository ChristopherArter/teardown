<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Apu extends Model
{
    public $timestamps = false;

    
    public $price;
    public $post_type = 'apu';

    public function user()
    {

        return $this->belongsTo(\App\User::class, 'owned_by', 'id');
    }

    public function invoice()
    {
        return $this->belongsTo(\App\Invoice::class);
    }

    public function expired()
    {

        if ($this->status == 'expired') {
            return true;
        }
    }


    // Renew APU
    public function renew()
    {

        $this->days_to_go = 30;
        $this->save();
    }


    public function showExpireDate()
    {


        $daysToGo = $this->days_to_go;
        $today = Carbon::now();
    }

    public function isActive()
    {

        if ($this->status == 'publish') {
            return true;
        }
    }

    function __construct()
    {

        $this->price = config('stripe.apu.price');
    }
}
