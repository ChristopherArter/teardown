<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Invoice;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;

class Invoice extends Model
{
//	public $timestamps = false;

    public $api_key;
    public $user;
    private $customer;
    public $items;
    public $order;
    public $invoice_num;

    public function apus()
    {
        return $this->hasMany(\App\Apu::class);
    }

        public function lineItems()
    {
        return $this->hasMany(\App\InvoiceLineItem::class);
    }

    public function user()
    {

        return $this->hasOne(\App\User::class, 'id', 'td_user_id');
    }


    public static function generatePDF($id)
    {

        $invoiceInput = Invoice::find($id);
        $invoiceId = $invoiceInput->stripe_invoice_id;

        $invoiceInput->invoice_number = $invoiceInput->id + 100;

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $getInvoice = \Stripe\Invoice::retrieve($invoiceId);

        $invoice = collect($getInvoice);
        $invoice->toArray();

        $pdf = PDF::loadView('invoices.invoicemaster', [

            'invoice'=>$invoice,
            'items' => $invoice['lines']['data'],
            'tdInvoice'=>$invoiceInput,
            'user' => Auth::user()

        ]);
        return $pdf->download('Teardown.AERO_Invoice_'.$invoiceInput['invoice_number'].'.pdf');
    }

    public function getLines()
    {

        // $apus = Apu::where('invoice_id','=', $this->id);

        // return $apus;
        


        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $getInvoice = \Stripe\Invoice::retrieve($this->stripe_invoice_id);

        $invoice = collect($getInvoice);
        $invoice->toArray();


        return $invoice['lines']['data'];
    }
}
