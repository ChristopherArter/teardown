<?php

namespace App;
use Laravel\Scout\Searchable;

use Illuminate\Database\Eloquent\Model;

class MarketPost extends Model
{


	    use Searchable;
    public $timestamps = false;

    public function favorites()
    {

        return $this->hasMany('App/Favorites');
    }

    public function user()
    {

        return $this->belongsTo(\App\User::class, 'id', 'author_id');
    }
}
