var elixir = require('laravel-elixir');
elixir.config.sourcemaps = false;
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

   mix.sass('app.scss')
   .combine(['jquery.js',
               'bootstrap.js',
               'datatables.js',
               'app.js', 
   				'icheck.min.js',
   				'home.js',
   				'market.js',
   				'messages.js',
   				'newListings.js',
   				'/vendor/jquery.validate.js',
   				'/vendor/jquery.vide.min.js',
   				'/vendor/parsley.js'

   				], 'public/js/app.js');

});


