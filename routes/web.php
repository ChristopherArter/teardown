<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Notifications\ExpirationWarning;

    // Home Page route

    // Route::get('/', function () {





    //         return route('page.home');


    // })->name('home');









    /**
     *
     *  AUTH MIDDLEWARE
     *
     *
     */


    Route::group(['middleware' => 'auth'], function () {

        // All my routes that needs a logged in user
        Route::get('/checkout', 'NewListingsController@showPaymentForm')->name('paymentform');

        // Dashboard Routes
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        // Shows order history
        Route::get('dashboard/billing/', 'DashboardController@billing')->name('dashboard.billing');

        // Displays the listings management - Note: This will be changed to new routes for each listing type.
        Route::get('dashboard/listings', 'DashboardController@listings')->name('dashboard.listings');


                /**
                 *  Credit Card Stripe Routes
                 */
        
                // AJAX route for CCs
                Route::get('dashboard/ccAjax', 'DashboardController@ccAjax')->name('dashboard.ccAjax');
                
                // Route to handle setting stripe default
                Route::post('dashboard/newdefault', 'DashboardController@setNewDefault')->name('dashboard.newDefault');
                
                // Add a credit card
                Route::post('dashboard/addcreditcard', 'DashboardController@addCreditCard')->name('dashboard.addCreditCard');

                // Delete Credit Cards
                Route::post('dashboard/deletecard', 'DashboardController@deleteCard')->name('dashboard.deleteCard');


        // Show invoices
        Route::get('dashboard/invoices', 'DashboardController@invoices')->name('dashboard.invoices');

        // Route to download an invoice by ID
        Route::get('/dashboard/invoices/download/{id}', 'InvoiceController@download')->name('dashboard.billing.download');

                /**
                 *
                 * User Profile Routes
                 *
                 */

                //Show user profile
                Route::get('dashboard/profile', 'DashboardController@profile')->name('dashboard.profile');
                
                // Update user data
                Route::post('dashboard/profile/update', 'DashboardController@updateUser')->name('dashboard.updateuser');

                // Remove the avatar file route
                Route::get('dashboard/profile/removeavatar', 'DashboardController@removeAvatar')->name('dashboard.removeavatar');


        // Show notifications
        Route::get('dashboard/notifications', 'DashboardController@messages')->name('dashboard.messages');

        // Messages controller
        Route::resource('message', 'MessageController');

        Route::get('dashboard/membership', 'DashboardController@membership')->name('dashboard.membership');
        Route::post('dashboard/membership/cancel/process', 'DashboardController@cancelSubscription')->name('dashboard.membership.cancel');

            /**
             *  Active Search Routes - Note: This is still in alpha stage. Not operational at this time.
             */
            
            // Route::get('dashboard/activesearch','DashboardController@activesearch')->name('dashboard.activesearch');
            // Route::post('dashboard/activesearch/create','DashboardController@activeSearchCreate')->name('dashboard.activesearchcreate');

        // Orders Resource
        Route::resource('orders', 'OrderController');
        // Route::resource('apus', 'MessageController');

        // Aircraft Resource 
        Route::resource('aircraft', 'AircraftController');

        //Read Message Post - Note: This is a legacy AJAX handler
        Route::post('dashboard/message/read', 'MessageController@readMessage');

        // Invoices Resource
        Route::resource('invoices', 'InvoiceController@index');

        // Favorite Resource <3
        Route::resource('favorite', 'FavoritesController@index');

       // Page Resource
        Route::resource('page', 'PageController');

        // Home page
        Route::get('/', 'PageController@home')->name('home');



        // New Listings Routes
        Route::get('/new', 'NewListingsController@index')->name('newlisting');
        Route::get('/new/apu', 'NewListingsController@apu')->name('newapulisting');
    });




    /**
     *
     *      STRIPE
     *
     *      Note: This runs through a second middleware called a stripe_check, which ensures the user has a default payment method set up before *      completing
     *
     */



    // Show checkout page
    Route::get('/checkout', 'NewListingsController@showPaymentForm')->name('paymentform')->middleware('auth', 'stripe_check');

    // Process Payment
    Route::post('/checkout/process', 'NewListingsController@processPayment')->name('processpayment')->middleware('auth', 'stripe_check');

    // Show Upgrade Page
    Route::get('dashboard/membership/upgrade', 'DashboardController@showUpgrade')->name('dashboard.membership.showupgrade')->middleware('auth', 'stripe_check');

    // Process Membership Upgrade
    Route::post('dashboard/membership/upgrade/process', 'DashboardController@processUpgrade')->name('dashboard.membership.processupgrade')->middleware('auth', 'stripe_check');




    // STRIPE WEBHOOK LISTENER - Note: This receives all requests from Stripe
    Route::post('stripe/webhook', 'StripeController@handleWebHook');



    /**
     *    /END STRIPE
     */




    // Search Results
    Route::get('search', 'SearchController@search')->name('search');

    // Log out
    Route::get('/logout', function () {

        Auth::logout();
        return redirect('/login')->with('success', 'successfully logged out');
    })->name('logout');


    // APU Resource Route - Note: This needs to be changed to partial routes, for auth purposes.
    Route::resource('apus', 'ApuController');

    // Engine Resource Route - Note: Ditto above.
    Route::resource('engines', 'EngineController');

    // Legacy Market routes that I may still use
    Route::get('market/ajax', 'MarketPostController@marketAjax');

    // Market Posts Resource Route
    Route::resource('market', 'MarketPostController');

    // Aircraft controller. Not sure why it's commented out, but now that it is, I'm affraid to uncomment.
    //Route::resource('aircrafts','AircraftController.php');



    //Email Signup Route
    
    Route::post('/emailoptin', 'EmailSignupController@signUp')->name('email.signup');




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


    Route::auth();

//Route::get('/', 'HomeController@index')->name('home');

    Auth::routes();

    Route::get('/home', 'HomeController@index');
