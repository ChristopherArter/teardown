<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveSearchFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_searches', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->json('params');
            $table->dateTime('created_at');
            $table->dateTime('last_queried_at')->nullable();
            $table->boolean('match_found')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
