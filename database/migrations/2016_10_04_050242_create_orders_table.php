<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->increments('id');
            $table->string('stripe_order_id');
            $table->string('user_id');
            $table->integer('total')->nullable();
            $table->string('status')->nullable();
            $table->integer('apu_id');
            $table->integer('engine_id');
            $table->dateTime('created_at');
            $table->dateTime('last_queried_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
