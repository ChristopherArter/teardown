<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarketPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('market_title');
            $table->mediumText('market_body');
            $table->integer('author_id');
            $table->bigInteger('views')->nullable();
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_posts', function (Blueprint $table) {
            //
        });
    }
}
