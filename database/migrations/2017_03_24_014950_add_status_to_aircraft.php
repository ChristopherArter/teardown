<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToAircraft extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft', function (Blueprint $table) {

            $table->string('status')->nullable();
            $table->string('make')->nullable();
            $table->mediumText('avionics')->nullable();
            $table->mediumText('landing_gear')->nullable();
            $table->mediumText('engines')->nullable();
            $table->mediumText('apu')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
