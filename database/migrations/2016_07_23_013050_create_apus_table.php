<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apus', function (Blueprint $table) {
            $table->increments('id');

            // Basic Details
            $table->string('model');
            $table->string('serial');
            $table->string('part_number');
            $table->string('sale_type');
            $table->string('location')->nullable();
            $table->text('details');


            // Condition, Tag & Trace
            $table->string('condition', 20);
            $table->string('tagged_by')->nullable();
            $table->date('tag_date')->nullable();
            $table->string('trace');


            // Cycle information
            $table->string('csn')->nullable();
            $table->string('tsn')->nullable();
            $table->string('cslsv')->nullable();
            $table->string('tslsv')->nullable();

            // User
            $table->integer('owned_by');

            // Subscription
            $table->integer('days_to_go');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
