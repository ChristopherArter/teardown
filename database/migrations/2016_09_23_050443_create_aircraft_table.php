<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAircraftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircraft', function (Blueprint $table) {
            $table->increments('id');

            // Basic Details
            $table->string('model');
            $table->string('serial');
            $table->string('tail_number')->nullable();
            $table->string('location')->nullable();
            $table->text('ex_airline');

            // User
            $table->integer('owned_by');

            // Subscription
            $table->integer('days_to_go')->default(90);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
