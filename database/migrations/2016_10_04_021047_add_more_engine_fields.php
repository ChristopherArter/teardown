<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreEngineFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('engines', function (Blueprint $table) {

            $table->string('status')->nullable();
            $table->string('minor_variant')->after('model')->nullable();
            $table->string('minipack')->nullable();
            $table->boolean('sale')->nullable();
            $table->boolean('lease')->nullable();
            $table->boolean('exchange')->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
