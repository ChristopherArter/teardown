<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeApuToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apus', function (Blueprint $table) {

            // Basic Details
            $table->string('model')->nullable()->change();
            $table->string('serial')->nullable()->change();
            $table->string('part_number')->nullable()->change();
            $table->string('sale_type')->nullable()->change();
            $table->string('location')->nullable()->change();
            $table->text('details')->nullable()->change();
        
        
            // Condition, Tag & Trace
            $table->string('condition', 20)->nullable()->change();
            $table->string('tagged_by')->nullable()->change();
            $table->date('tag_date')->nullable()->change();
            $table->string('trace')->nullable()->change();
        
        
            // Cycle information
            $table->string('csn')->nullable()->change();
            $table->string('tsn')->nullable()->change();
            $table->string('cslsv')->nullable()->change();
            $table->string('tslsv')->nullable()->change();
        
            // Subscription
            $table->integer('days_to_go')->default(30)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
