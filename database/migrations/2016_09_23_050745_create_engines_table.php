<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnginesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engines', function (Blueprint $table) {
            $table->increments('id');

            // Basic Details
            $table->string('model')->nullable();
            $table->string('serial')->nullable();
            $table->string('part_number')->nullable();
            $table->string('sale_type')->nullable();
            $table->string('location')->nullable();
            $table->text('details')->nullable();


            // Condition, Tag & Trace
            $table->string('condition', 20);
            $table->string('tagged_by')->nullable();
            $table->date('tag_date')->nullable();
            $table->string('trace');


            // Cycle information
            $table->string('csn')->nullable();
            $table->string('tsn')->nullable();
            $table->string('cslsv')->nullable();
            $table->string('tslsv')->nullable();

            // User
            $table->integer('owned_by');

            // Subscription
            $table->integer('days_to_go');

            // Manifests
            $table->integer('manifest_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
